mutable struct MHPathSolver <: AbstractSolver
    bh::BHierarchy
    loop::Int

    MHPathSolver(bh::BHierarchy) = new(bh, 0)
end

@inline function _inference!(SS::MHPathSolver, burnin::Int)
    bh = SS.bh
    initialize!(bh)

    print_point::Int = burnin <= 20 ? 2 : 100

    num_var = ceil(bh.n/VAR_PARTS) |> Int
    zs = NodeVec(undef, bh.L+1)
    zps = NodeVec()
    newθs = Vector{DistParameters}(undef, num_var)
    logps::DoubleVec = zeros(num_var)

    hyp_priors = NNBHierPar()

    SS.loop = 0
    #mlhkds::DoubleVec = zeros(burnin)
    for ℓ = 1:burnin
        ℓ % print_point == 1 && @info "loop $ℓ......"
        infer!(bh, SS, zs, zps, newθs, logps, nothing, false)
        #mlhkds[ℓ] = log_mcle_v2(bh)
    end

    #npzwrite("output/mlhkds3.npy", mlhkds)
    @info "the last loop $burnin finished!"
end

@inline function _inference_conv!(SS::MHPathSolver, burnin::Int,
                                  run_index::Int)
    bh = SS.bh
    initialize!(bh)

    print_point::Int = burnin <= 20 ? 2 : 100

    num_var = ceil(bh.n/VAR_PARTS) |> Int
    zs = NodeVec(undef, bh.L+1)
    zps = NodeVec()
    newθs = Vector{DistParameters}(undef, num_var)
    logps::DoubleVec = zeros(num_var)

    df = DataFrame(round=Int[], 
                   loglkhd=Float64[],
                   logprior=Float64[], 
                   logpost=Float64[])

    loglkhd = log_lkhd(bh)
    logprior = log_prior(bh; if_β=true, if_θ=true)
    push!(df, [0, loglkhd, logprior, loglkhd+logprior])

    for i = 1:burnin
        i % print_point == 1 && @info "loop $i......"
        infer!(bh, SS, zs, zps, newθs, logps)

        if burnin % 50 == 0
            loglkhd = log_lkhd(bh)
            logprior = log_prior(bh; if_β=true, if_θ=true)
            push!(df, [i, loglkhd, logprior, loglkhd+logprior])
        end
    end
    @info "the last loop $burnin finished!"

    savedf(df, joinpath(opath, "convergence_$run_index.csv"))
    @info("The file has been stored!")
end

@inline function infer!(bh::BHierarchy,
                        solver::MHPathSolver,
                        zs::NodeVec,
                        zps::NodeVec,
                        newθs::Vector{DistParameters},
                        logps::DoubleVec,
                        hyp_priors::Union{Nothing, NIWBHierPar, NNBHierPar},
                        sample_hyp_priors::Bool=true)
    solver.loop += 1
    old_zs::NodeVec = []
    old_k4x = 0
    w_list = Queue{Tuple{Int, Node}}()

    inds = randperm(bh.n)

    bhp = BHierarchy(bh.X, bh.α, bh.γ, bh.η, bh.G, bh.H, bh.L, copy(bh.lkhds))

    logprior = loglkhd = logpost = logprior_p = loglkhd_p = logpost_p = 0.
    acc, chance, num_acc = 0., 0., 0

    logprior = log_prior(bh)
    @inbounds for ind in inds
        old_zs = get_zps(bh.z_x, ind)
        old_k4x = findfirst(bh.x_k[ind, :] .> 0)

        loglkhd = log_lkhd_path(ind, bh)
        logpost = logprior + loglkhd
        clean_ss!(bh, old_k4x, ind)
        Q = q(old_zs, bh)

        semicopy!(bhp, bh)

        check_mat!(bhp)

        nCRP_path!(zs, zps, bhp, ind)

        Q_p = q(zs, bhp)
        loglkhd_p = log_lkhd_path(ind, zs, bhp)
        logprior_p = log_prior(bhp)

        logpost_p = logprior_p + loglkhd_p

        acc = min(0, log_proposal(logpost, logpost_p, Q, Q_p))

        chance = rrand()
        if log(chance) > acc
            add_ss!(bh, ind, old_k4x, old_zs)
        else
            z = last(zs)
            cmp_logps = zeros(bhp.K)
            cmp_logps[1:bhp.K-1] = component_log_pdfs(bhp.X[:, ind], bhp)
            cmp_logps[end] = bhp.lkhds[ind]
            cmp_logps .+= sl_log.(bhp.B[1:bhp.K, z])

            k4x = @> cmp_logps Vector rand_log_discrete

            k4x == bhp.K && (k4x = new_component2(bhp))

            @assert k4x != bhp.K "$k4x $(bhp.K)"

            bhp.x_k[ind, k4x] = 1
            bhp.n_k_x[k4x] += 1
            bhp.z_x[zs, ind] .= 1
            bhp.n_z_k[zs, k4x] .+= 1

            num_acc += 1
            semicopy!(bh, bhp)
            logprior = logprior_p
        end
    end
    update_βs!(bh, w_list)
    update_θs!(newθs, logps, bh, bh.H)

    if sample_hyp_priors == true && solver.loop <= 300
        sample_hyperparameters!(bh, hyp_priors)
        batch_log_mgnl_lkhds!(bh.lkhds, bh.X, bh.G, bh.H)
    end
end

log_proposal(logpost, logpost_p, Q, Q_p) = logpost_p - logpost + Q - Q_p

component_log_pdfs(x::DoubleVec, bh::BHierarchy) =
    map(θ -> logp(x, bh.G, θ), bh.θs)

function update_θs!(θs::Vector{DistParameters},
                    logps::DoubleVec, bh::BHierarchy, H::CNIW)
    @assert H == bh.H
    nz_ks = findall(!iszero, bh.n_z_k[1, :])
    λ, ν= bh.H.λ, bh.H.ν
    μ0 = bh.H.μ0
    Ψ = bh.H.Ψ

    n, λ_, ν_ = 0, 0, 0
    μ_ = zeros(bh.dim)
    Ψ_ = zeros(bh.dim, bh.dim)
    @inbounds @simd for ind = 1:length(nz_ks)
        k_ind = nz_ks[ind]

        d_inds = findall(!iszero, bh.x_k[:, k_ind])

        n = length(d_inds)
        @assert n > 0

        λ_ = λ + n
        ν_ = ν + n

        m = mean(bh.X[:, d_inds], dims=2) |> vec
        μ_ .= (λ * μ0 .+ n * m) / λ_
        Ψ_ .= @views Ψ +
            sum((bh.X[:, d_ind] .- m) * (bh.X[:, d_ind] .- m)' for
                d_ind in d_inds) +
                    λ * n / λ_ * (m - μ0) * (m - μ0)'

        ns = CNIW(μ_, Ψ_ |> Symmetric |> Matrix |> PDMat, λ_, ν_) |> rand

        bh.θs[k_ind].μ .= ns[1]
        bh.θs[k_ind].Σ .= ns[2]
    end
end

@inline function update_θs!(θs::Vector{DistParameters},
                            logps::DoubleVec, bh::BHierarchy, H::CMvNormal)
    @assert bh.H == H

    nz_ks = findall(!iszero, bh.n_z_k[1, :])
    μ = bh.H.μ
    H_Σinv = bh.H.Σinv
    G_Σinv = bh.G.Σinv

    n = 0
    μ_ = zeros(bh.dim)
    Σ_ = zeros(bh.dim, bh.dim)
    m = zeros(bh.dim)
    k_ind = 0
    @inbounds @simd for ind = 1:length(nz_ks)
        k_ind = nz_ks[ind]

        d_inds = findall(!iszero, bh.x_k[:, k_ind])

        n = length(d_inds)
        @assert n > 0

        m .= mean(bh.X[:, d_inds], dims=2) |> vec
        Σ_ .= @views inv(H_Σinv + n * G_Σinv)
        μ_ .= @views Σ_ * (H.Σinvμ + n * G_Σinv * m)

        # bh.θs[k_ind].μ .= CMvNormal(μ_, Σ_) |> rand |> vec

        bh.θs[k_ind].μ .= MvNormal(μ_, Σ_) |> rrand |> vec
    end
end

"""
- bh: an instance of the Bayes Hierarchy
- w_list: a queue structured waiting list
"""
@inline function update_βs!(bh::BHierarchy,
                            w_list::Queue=Queue{Tuple{Int, Node}}())
    zps = NodeVec()
    ℓ, z = 0, 1

    n_k_p::DoubleVec = zeros(bh.K)
    n_k_p[1:end-1] = bh.n_z_k[z, 1:bh.K-1]
    n_k_p[end] = bh.α

    bh.B[:, z] .= 0
    mask = n_k_p .> TOL
    bh.B[mask, z] = @> n_k_p[mask] Vector Dirichlet rrand

    enqueue!(w_list, (ℓ, z))

    dropzeros!(bh.adjmat)
    @inbounds while !isempty(w_list)
        ℓ, z = dequeue!(w_list)
        get_zps!(zps, bh.adjmat, z)

        inds = findall(!iszero, bh.n_z_x[zps])
        isempty(inds) && continue

        for zp in zps[inds]
            n_k_p[:] = @. bh.n_z_k[zp, 1:bh.K] + bh.B[1:bh.K, z] * bh.η
            mask = n_k_p .> TOL
            bh.B[:, zp] .= 0
            bh.B[mask, zp] = @> n_k_p[mask] Dirichlet rrand
            enqueue!(w_list, (ℓ+1, zp))
        end
    end

    # println(bh.B[:, 1])
end


@inline function sample_hyperparameters!(bh::BHierarchy, hyp_priors::NIWBHierPar)
    α, γ0, η, ν, λ = bh.α, bh.γ, bh.η, bh.H.ν, bh.H.λ

    old_prior = pars_logpdf(bh, hyp_priors)
    old_post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    resample!(hyp_priors)
    assign!(hyp_priors, bh)
    prior = pars_logpdf(bh, hyp_priors)
    post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    u = rrand()

    if sl_log(u) > min(0, post - old_post + old_prior - prior)
        # if the proposal is not accepted, reset to its original parameter values
        bh.α, bh.γ, bh.η, bh.H.ν, bh.H.λ = α, γ0, η, ν, λ
    else
        println("hyper parameters accepted...")
    end
end


@inline function sample_hyperparameters!(bh::BHierarchy, hyp_priors::NNBHierPar)
    α, γ0, η, Σ = bh.α, bh.γ, bh.η, bh.G.Σ

    old_prior = pars_logpdf([α, γ0, η, Σ[1, 1]], hyp_priors)
    old_post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    resample!(hyp_priors)
    assign!(hyp_priors, bh)
    prior = pars_logpdf(bh, hyp_priors)
    post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    u = rrand()

    if sl_log(u) > min(0, post - old_post + old_prior - prior)
        # if the proposal is not accepted, reset to its original parameter values
        bh.α, bh.γ, bh.η = α, γ0, η
        assign_Σ!(bh.G, Σ)
    else
        println("hyper parameters accepted...")
    end
end

# function sample_hyperparameters!(bh::BHierarchy, hyp_priors::NIWBHierPar)
#     α, γ0, η, ν, λ = bh.α, bh.γ, bh.η, bh.H.ν, bh.H.λ
#     resample!(hyp_priors)
    #
    # old_prior = dlogpdf(hyp_priors.G4α, α)
    # old_post = log_posterior(bh; if_hier=true, if_β=false, if_θ=false)
    #
    # bh.α = hyp_priors.α
    # prior = dlogpdf(hyp_priors.G4α, bh.α)
    # post = log_posterior(bh; if_hier=true, if_β=false, if_θ=false)
    #
    # u = rrand()
    # if log(u) > min(0, post - old_post + old_prior - prior)
    #     # if the proposal is not accepted, reset to its original parameter values
    #     bh.α = α
    # # else
    # #     println("hyper parameters α accepted...")
    # end
    #
    # # .......
    #
    # old_prior = dlogpdf(hyp_priors.G4γ, γ0)
    # old_post = log_posterior(bh; if_hier=true, if_β=true, if_θ=false)
    #
    # bh.γ = hyp_priors.γ
    # prior = dlogpdf(hyp_priors.G4γ, bh.γ)
    # post = log_posterior(bh; if_hier=true, if_β=true, if_θ=false)
    #
    # u = rrand()
    # if log(u) > min(0, post - old_post + old_prior - prior)
    #     # if the proposal is not accepted, reset to its original parameter values
    #     bh.γ = γ0
    # # else
    # #     println("hyper parameters γ accepted...")
    # end

    # .......

#     old_prior = dlogpdf(hyp_priors.G4η, η)
#     old_post = log_posterior(bh; if_hier=true, if_β=true, if_θ=false)
#
#     bh.η = hyp_priors.η
#     prior = dlogpdf(hyp_priors.G4η, bh.η)
#     post = log_posterior(bh; if_hier=true, if_β=true, if_θ=false)
#
#     u = rrand()
#     if log(u) > min(0, post - old_post + old_prior - prior)
#         # if the proposal is not accepted, reset to its original parameter values
#         bh.η = η
#     # else
#     #     println("hyper parameters η accepted...")
#     end
#
#     # .......
#
#     old_prior = dlogpdf(hyp_priors.G4λ, λ)
#     old_post = log_posterior(bh; if_hier=true, if_β=false, if_θ=true)
#
#     bh.H.λ = hyp_priors.λ
#     prior = dlogpdf(hyp_priors.G4λ, bh.H.λ)
#     post = log_posterior(bh; if_hier=true, if_β=false, if_θ=true)
#
#     u = rrand()
#     if log(u) > min(0, post - old_post + old_prior - prior)
#         # if the proposal is not accepted, reset to its original parameter values
#         bh.H.λ = λ
#     # else
#     #     println("hyper parameters λ accepted...")
#     end
#
#     # .......
#
#     old_prior = dlogpdf(hyp_priors.G4ν, ν - (bh.dim - 1))
#     old_post = log_posterior(bh; if_hier=true, if_β=false, if_θ=true)
#
#     bh.H.ν = hyp_priors.ν
#     prior = dlogpdf(hyp_priors.G4ν, bh.H.ν - (bh.dim - 1))
#     post = log_posterior(bh; if_hier=true, if_β=false, if_θ=true)
#
#     u = rrand()
#     if log(u) > min(0, post - old_post + old_prior - prior)
#         # if the proposal is not accepted, reset to its original parameter values
#         bh.H.ν = ν
#     # else
#     #     println("hyper parameters ν accepted...", bh.H.ν)
#     end
# end


@inline function initialize!(bh::BHierarchy)
    zs, zps = NodeVec(undef, bh.L+1), NodeVec()

    G = GEM(bh.γ)
    βs::DoubleVec = @>> G take_stkbrk_gt(bh.u0) collect
    bh.K = length(βs)
    bh.K > bh.n && (bh.K = bh.n)

    βs[bh.K] = sum(βs[bh.K:end])
    bh.B[1:bh.K, 1] = βs[1:bh.K]
    bh.max_node += 1
    resize!(bh.θs, bh.K-1)
    # sample_θs!(bh.θs, bh.H, bh.K-1)

    # inds = sample(1:bh.n, bh.n, replace=false)
    @inbounds @simd for k = 1:bh.K-1
        ind = rrand(1:bh.n)
        # ind = inds[k]
        bh.θs[k] = NNParameters(bh.X[:, ind] |> Vector)
    end

    for i = 1:bh.n
        check_mat!(bh)
        nCRP_infer!(zs, zps, bh, i)
    end

    @info "Initiazation is finished..."
end

function sample_θs!(θs::Vector{DistParameters},
                    H::AbstractDistribution, K::Int)
    resize!(θs, K)
    map!(_ -> rand_param(H), θs, 1:K)
end

function sample_θs!(θs::Vector{DistParameters},
                    H::AbstractDistribution)
    map!(_ -> rand_param(H), θs, 1:length(θs))
end


function rand!(bh::BHierarchy)
    G = GEM(bh.γ)
    βs = @>> G take_stkbrk_gt(TOL) collect

    bh.K = length(βs) < bh.n ? length(βs) : bh.n
    bh.B[1:bh.K, 1] = βs[1:bh.K]
    sample_θs!(bh.θs, bh.H, bh.K)

    k4x, zs, zps = 0, NodeVec(undef, bh.L+1), NodeVec()
    for i = 1:bh.n
        nCRP!(zs, zps, bh, i)
        k4x = get_component(bh, i)
        k4x == bh.K && (k4x = new_component2(bh))
        bh.X[:, i] = rand(bh.G, bh.θs[k4x])
    end
end

function rand(bh::BHierarchy, n::Int)
    X::DoubleMat = zeros(n, bh.dim)
    k4x, zs, zps = 0, NodeVec(undef, bh.L+1), NodeVec()
    @inbounds for i = 1:n
        nCRP!(zs, zps, bh, i)
        k4x = get_component(bh, i)
        k4x == bh.K && (k4x = new_component2(bh))
        X[i, :] .= rand(bh.G, bh.θs[k4x]) |> vec
    end

    X
end

"""
Create a new component and update the corresponding
β for each node in the hierarchy
"""
@inline function new_component(bh::BHierarchy)
    w_queue = Queue{Node}()

    z = 1
    enqueue!(w_queue, z)

    β = Beta(1, bh.γ) |> rrand
    bh.B[bh.K+1, z] = bh.B[bh.K, z]
    @. bh.B[bh.K:bh.K+1, z] *= [β, 1 - β]

    βs, βs_p = DoubleVec(), DoubleVec()
    a, b = 0., 0.
    while !isempty(w_queue)
        z = dequeue!(w_queue)

        zps = get_zps(bh.adjmat, z)
        a, b = bh.η * bh.B[bh.K:bh.K+1, z]

        (a > 0 && b > 0) || continue
        resize!(βs, length(zps))
        resize!(βs_p, length(zps))

        rrand!(Beta(a, b), βs)
        βs_p[:] = 1 .- βs

        bh.B[bh.K+1, zps] = bh.B[bh.K, zps]
        @. bh.B[bh.K, zps] *= βs
        @. bh.B[bh.K+1, zps] *= βs_p

        for zp in zps enqueue!(w_queue, zp) end
    end
    bh.K += 1
    push!(bh.θs, rand_param(bh.H))
end

"""
Create a new component and update the corresponding
β for each node in the hierarchy

This function needs to be further polished
"""
@inline function new_component2(bh::BHierarchy)
    w_queue = Queue{Node}()

    z = 1
    enqueue!(w_queue, z)

    # println(bh.B[:, z], " ", bh.K, " ", bh.n, "   ", sum(bh.B[:, z]))
    # println(bh.α, "  ", bh.γ, "  ", bh.η)
    pos = bh.K == bh.n ? findfirst(iszero, bh.B[:, z]) : bh.K
    inds = bh.K == bh.n ? [pos, bh.K] : [bh.K, bh.K+1]

    β = rrand(Beta(1, bh.γ))
    bh.B[pos, z] = bh.B[bh.K, z]

    @. bh.B[inds, z] *= [β, 1 - β]

    βs, βs_p = DoubleVec(), DoubleVec()
    a, b = 0., 0.
    while !isempty(w_queue)
        z = dequeue!(w_queue)

        zps = get_zps(bh.adjmat, z)
        a, b = bh.η * bh.B[inds, z]

        (a > 0 && b > 0) || continue
        resize!(βs, length(zps))
        resize!(βs_p, length(zps))

        rrand!(Beta(a, b), βs)
        βs_p[:] = 1 .- βs

        bh.B[pos, zps] = bh.B[bh.K, zps]

        if bh.K != bh.n
            @. bh.B[bh.K, zps] *= βs
            @. bh.B[pos, zps] *= βs_p
        else
            @. bh.B[pos, zps] += bh.B[pos, zps] * βs
            @. bh.B[bh.K, zps] *= βs_p
        end
        # for zp in zps enqueue!(w_queue, zp) end
        foreach(zp -> enqueue!(w_queue, zp), zps)
    end

    # println(" =======   ", bh.K, " ", pos, " ", length(bh.θs))
    if bh.K < bh.n
        bh.K += 1
        push!(bh.θs, rand_param(bh.H))
    else
        bh.θs[pos] = rand_param(bh.H)
    end

    @assert length(bh.θs) == bh.K - 1
    @assert pos < bh.K "$(bh.K)   $(length(bh.θs))"
    return pos
end

function check_mat!(bh::BHierarchy)
    bh.max_node + bh.L ≥ length(bh.n_z_x) &&
        ifelse(nnz(bh.n_z_x) + bh.L < length(bh.n_z_x),
               adjust_node_matrix!(bh), expand_bh_mat!(bh))
end

function new_node(bh::BHierarchy)
    bh.max_node += 1
    bh.max_node
end

get_component(bh::BHierarchy, i::Int) = @>> bh.x_k[i, :] findfirst(!iszero)
