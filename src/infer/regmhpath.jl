mutable struct RegMHPathSolver <: AbstractSolver
    bh::VBMCMCHierarchy
    loop::Int

    RegMHPathSolver(bh::VBMCMCHierarchy) = new(bh, 0)
end

@inline function _inference!(S::RegMHPathSolver, burnin::Int,
                             if_save_global_comps::Bool=false)
    bh = S.bh
    initialize!(bh)

    print_point::Int = burnin <= 20 ? 2 : 5000

    zs = NodeVec(undef, bh.L+1)
    zps = NodeVec()

    hyp_priors = NNBHierPar()

    S.loop = 0
    @inbounds for ℓ = 1:burnin
        ℓ % print_point == 1 && @info "loop $ℓ......"
        infer!(S, zs, zps, nothing, false)
    end

    if_save_global_comps && save_global_comps(RMHS.bh)

    #npzwrite("output/mlhkds3.npy", mlhkds)
    @info "the last loop $burnin finished!"
end

@inline function _inference_conv!(S::RegMHPathSolver, burnin::Int,
                                  run_index::Int)
    bh = S.bh
    initialize!(bh)

    print_point::Int = burnin <= 20 ? 2 : 100

    num_var = ceil(bh.n/VAR_PARTS) |> Int
    zs = NodeVec(undef, bh.L+1)
    zps = NodeVec()
    newθs = Vector{DistParameters}(undef, num_var)
    logps::DoubleVec = zeros(num_var)

    df = DataFrame(round=Int[], loglkhd=Float64[],
                   logprior=Float64[], logpost=Float64[])

    loglkhd = log_lkhd(bh)
    logprior = log_prior(bh; if_β=true, if_θ=true)
    push!(df, [0, loglkhd, logprior, loglkhd+logprior])

    for i = 1:burnin
        i % print_point == 1 && @info "loop $i......"
        infer!(bh, SS, zs, zps, newθs, logps)

        if burnin % 50 == 0
            loglkhd = log_lkhd(bh)
            logprior = log_prior(bh; if_β=true, if_θ=true)
            push!(df, [i, loglkhd, logprior, loglkhd+logprior])
        end
    end
    @info "the last loop $burnin finished!"

    savedf(df, joinpath(opath, "convergence_$run_index.csv"))
    @info("The file has been stored!")
end

@inline function infer!(S::RegMHPathSolver,
                        zs::NodeVec,
                        zps::NodeVec,
                        hyp_priors::Union{Nothing, NIWBHierPar, NNBHierPar},
                        sample_hyp_priors::Bool=false)
    bh = S.bh

    length(zs) == bh.L+1 || resize!(zs, bh.L+1)

    s::Node = 0
    z::Node = 0
    ℓ::Int = 0
    pse_lhkd::Float64 = 0.

    old_zs::NodeVec = []
    old_k4x::Int = 0
    k4x::Int = 0

    bhp = VBMCMCHierarchy(bh.X, bh.α, bh.γ,
                          bh.η, bh.G, bh.H, bh.L,
                          bh.C, bh.ϵ0, bh.ν0, false;
                          if_reg=bh.if_reg)
    bhp.lkhds .= bh.lkhds

    # logprior = loglkhd = logpost = logprior_p = loglkhd_p = logpost_p = 0.
    logprior = loglkhd = logprior_p = loglkhd_p = 0.

    acc, chance, num_acc = 0., 0., 0


    @inbounds for d_ind in randperm(bh.n)
        old_zs = get_zps(bh.z_x, d_ind)
        old_k4x = findfirst(bh.x_k[d_ind, :] .> 0)

        loglkhd = log_lkhd_path(d_ind, bh)
        if bh.if_reg
            pse_lhkd = compute_pseudo_lkhd(bh, d_ind, old_zs; if_λ=false)
            loglkhd += pse_lhkd
        end
        # logpost = loglkhd

        clean_ss!(bh, d_ind, old_k4x)

        semicopy!(bhp, bh)

        check_mat!(bhp)

        nCRP_path!(zs, zps, bhp, d_ind)

        loglkhd_p = log_lkhd_path(d_ind, zs, bhp)
        if bh.if_reg
            pse_lhkd, s, lz = pseudo_lkhd_n_assignments(bhp, d_ind, zs)
            loglkhd_p += pse_lhkd
        end
        # logpost_p = loglkhd_p

        acc = min(0, log_proposal(loglkhd, loglkhd_p, 0., 0.))

        chance = rrand()
        if sl_log(chance) > acc
            z = last(old_zs)
            k4x = sample_k!(bh, d_ind, z)
            @assert bh.B[k4x, z] > 0

            add_ss!(bh, d_ind, k4x, old_zs)
            bh.if_reg && update_λs!(bh, d_ind, old_zs)
        else
            z = last(zs)
            k4x = sample_k!(bhp, d_ind, z)
            @assert bhp.B[k4x, z] > 0

            # the sampling function has calculated for n_z_x
            # for computing the probability
            bhp.x_k[d_ind, k4x] = 1
            bhp.n_k_x[k4x] += 1
            bhp.z_x[zs, d_ind] .= 1
            bhp.n_z_k[zs, k4x] .+= 1

            semicopy!(bh, bhp)

            if bh.if_reg
                bh.ss[d_ind] = s
                bh.lzs[d_ind] = lz
                update_λs!(bh, d_ind, zs)
            end

            logprior = logprior_p
        end
    end
    update_global_variables!(bh)
    update_θs!(bh, bh.H)

    # if sample_hyp_priors == true && solver.loop <= 300
    #     sample_hyperparameters!(bh, hyp_priors)
    #     batch_log_mgnl_lkhds!(bh.lkhds, bh.X, bh.G, bh.H)
    # end

    bh.loop += 1
    S.loop += 1
end

function sample_k!(bh::VBMCMCHierarchy, d_ind::Int, z::Node)
    k_inds = findall(bh.B[1:bh.K-1, z] .> TOL)
    logps::DoubleVec = zeros(length(k_inds))
    component_log_pdfs!(bh, logps, k_inds, d_ind)

    push!(logps, bh.lkhds[d_ind])
    push!(k_inds, bh.K)
    logps .+= sl_log.(bh.B[k_inds, z])

    k_ind = @> logps rand_log_discrete(k_inds)
    k_ind == bh.K && (k_ind = new_component2(bh))

    return k_ind
end


@inline function path_prior(bh::VBMCMCHierarchy, d_ind::Int, zs::NodeVec; aftercleaning=true)
    logp = 0.
    @inbounds for ℓ in 2:length(zs)
        n_z = aftercleaning ? bh.n_z_x[zs[ℓ]] : bh.n_z_x[zs[ℓ]] - 1
        if n_z != 0
            # otherwise logp += 0
            n_zstar = aftercleaning ?
                bh.n_z_x[zs[ℓ-1]] : bh.n_z_x[zs[ℓ-1]] - 1
            n_zstar += bh.α

            logp += sl_log(n_z) - sl_log(n_zstar)
        end
    end
    return logp
end

component_log_pdfs(x::DoubleVec, bh::VBMCMCHierarchy) =
    map(θ -> logp(x, bh.G, θ), bh.θs)

function update_θs!(bh::VBMCMCHierarchy, H::CNIW)
    @assert H == bh.H
    nz_ks = findall(!iszero, bh.n_z_k[1, :])
    λ, ν= bh.H.λ, bh.H.ν
    μ0 = bh.H.μ0
    Ψ = bh.H.Ψ

    n, λ_, ν_ = 0, 0, 0
    μ_ = zeros(bh.dim)
    Ψ_ = zeros(bh.dim, bh.dim)
    @inbounds @simd for ind = 1:length(nz_ks)
        k_ind = nz_ks[ind]

        d_inds = findall(!iszero, bh.x_k[:, k_ind])

        n = length(d_inds)
        @assert n > 0

        λ_ = λ + n
        ν_ = ν + n

        m = mean(bh.X[:, d_inds], dims=2) |> vec
        μ_ .= (λ * μ0 .+ n * m) / λ_
        Ψ_ .= @views Ψ +
            sum((bh.X[:, d_ind] .- m) * (bh.X[:, d_ind] .- m)' for
                d_ind in d_inds) + λ * n / λ_ * (m - μ0) * (m - μ0)'

        ns = CNIW(μ_, Ψ_ |> Symmetric |> Matrix |> PDMat, λ_, ν_) |> rand

        bh.θs[k_ind].μ .= ns[1]
        bh.θs[k_ind].Σ .= ns[2]
    end
end

@inline function update_θs!(bh::VBMCMCHierarchy, H::CMvNormal)
    @assert bh.H == H

    nz_ks = findall(!iszero, bh.n_z_k[1, :])
    μ = bh.H.μ
    H_Σinv = bh.H.Σinv
    G_Σinv = bh.G.Σinv

    n = 0
    μ_ = zeros(bh.dim)
    Σ_ = zeros(bh.dim, bh.dim)
    m = zeros(bh.dim)
    k_ind = 0
    @inbounds @simd for ind = 1:length(nz_ks)
        k_ind = nz_ks[ind]

        d_inds = findall(!iszero, bh.x_k[:, k_ind])

        n = length(d_inds)
        @assert n > 0

        m .= mean(bh.X[:, d_inds], dims=2) |> vec
        Σ_ .= @views inv(H_Σinv + n * G_Σinv)
        μ_ .= @views Σ_ * (H.Σinvμ + n * G_Σinv * m)

        bh.θs[k_ind].μ .= MvNormal(μ_, Σ_) |> rrand |> vec
    end
end

"""
- bh: an instance of the Bayes Hierarchy
- w_list: a queue structured waiting list
"""
@inline function update_global_variables!(bh::VBMCMCHierarchy)
    w_list = Queue{Tuple{Int, Node}}()
    zps = NodeVec()
    ℓ, z = 0, 1

    μ_::DoubleVec = zeros(bh.dim)
    Σ_::DoubleMat = zeros(bh.dim, bh.dim)
    enqueue!(w_list, (ℓ, z))

    dropzeros!(bh.adjmat)
    @inbounds while !isempty(w_list)
        ℓ, z = dequeue!(w_list)
        zps = children(bh, z)

        update_β!(bh, z)
        (z != 1 && bh.if_reg) && update_η!(bh, z, ℓ, μ_, Σ_)

        inds = findall(bh.n_z_x[zps] .> 0)
        isempty(inds) && continue
        foreach(zp -> enqueue!(w_list, (ℓ+1, zp)), zps[inds])
    end
end

@inline function update_β!(bh::VBMCMCHierarchy, z::Node)
    z_star = parent(bh, z)

    n_k  = zeros(bh.K)
    @inbounds if z_star == 0
        n_k[1:end-1] .= bh.n_z_k[z, 1:bh.K-1]
        n_k[end] = bh.γ
    else
        n_k .= bh.n_z_k[z, 1:bh.K] .+ bh.B[1:bh.K, z_star] .* bh.η
    end

    mask = n_k .> TOL
    @inbounds bh.B[1:bh.K, z] .= 0
    @inbounds bh.B[mask, z] = @> n_k[mask] Vector Dirichlet rrand lnormalize(1)
end

@inline function sample_hyperparameters!(bh::VBMCMCHierarchy,
                                         hyp_priors::NIWBHierPar)
    α, γ0, η, ν, λ = bh.α, bh.γ, bh.η, bh.H.ν, bh.H.λ

    old_prior = pars_logpdf(bh, hyp_priors)
    old_post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    resample!(hyp_priors)
    assign!(hyp_priors, bh)
    prior = pars_logpdf(bh, hyp_priors)
    post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    u = rrand()

    if sl_log(u) > min(0, post - old_post + old_prior - prior)
        # if the proposal is not accepted, reset to its original parameter values
        bh.α, bh.γ, bh.η, bh.H.ν, bh.H.λ = α, γ0, η, ν, λ
    else
        println("hyper parameters accepted...")
    end
end


@inline function sample_hyperparameters!(bh::VBMCMCHierarchy,
                                         hyp_priors::NNBHierPar)
    α, γ0, η, Σ = bh.α, bh.γ, bh.η, bh.G.Σ

    old_prior = pars_logpdf([α, γ0, η, Σ[1, 1]], hyp_priors)
    old_post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    resample!(hyp_priors)
    assign!(hyp_priors, bh)
    prior = pars_logpdf(bh, hyp_priors)
    post = log_posterior(bh; if_hier=true, if_β=true, if_θ=true)

    u = rrand()

    if sl_log(u) > min(0, post - old_post + old_prior - prior)
        # if the proposal is not accepted, reset to its original parameter values
        bh.α, bh.γ, bh.η = α, γ0, η
        assign_Σ!(bh.G, Σ)
    else
        println("hyper parameters accepted...")
    end
end

@inline function initialize!(bh::VBMCMCHierarchy)
    zs, zps = NodeVec(undef, bh.L+1), NodeVec()

    G = GEM(bh.γ)
    βs::DoubleVec = @>> G take_stkbrk_gt(bh.u0) collect
    bh.K = length(βs)
    bh.K > bh.n && (bh.K = bh.n)

    z::Node = 0
    βs[bh.K] = 1 - sum(βs[1:bh.K-1])
    bh.B[1:bh.K, 1] .= βs[1:bh.K]
    bh.max_node += 1
    resize!(bh.θs, bh.K-1)

    @inbounds @simd for k = 1:bh.K-1
        ind = rrand(1:bh.n)
        bh.θs[k] = NNParameters(bh.X[:, ind] |> Vector)
        # bh.θs[k] = NNParameters(rand(bh.H) |> vec)
    end

    tmp_max_node = 0
    @inbounds for (_, d_ind) in enumerate(randperm(bh.n))
        check_mat!(bh)

        tmp_max_node = bh.max_node
        nCRP_infer!(zs, zps, bh, d_ind)

        if bh.if_reg
            # the first node of zs is always the root node
            @inbounds for z in zs[2:end-1]
                z > bh.max_node && sample_η_prior!(bh, z)
            end
        end
    end

    if bh.if_reg
        @inbounds for d_ind in 1:bh.n
            zs = findall(bh.z_x[:, d_ind] .> 0)
            _, s, lz = pseudo_lkhd_n_assignments(bh, d_ind, zs)
            bh.ss[d_ind] = s
            bh.lzs[d_ind] = lz
            update_λs!(bh, d_ind, zs)
        end
    end

    @info "Initiazation is finished..."
end

@inline function log_mle(bh::VBMCMCHierarchy)
    logmle = 0.
    logps::DoubleVec = zeros(bh.K)
    @inbounds for d_ind = 1:bh.n
        fill!(logps, -Inf)
        zs = findall(!iszero, bh.z_x[:, d_ind])
        k_inds = findall(bh.B[1:bh.K-1, zs[end]] .> 0)
        component_log_pdfs!(bh, logps, k_inds, d_ind)
        push!(k_inds, bh.K)
        logps[bh.K] = bh.lkhds[d_ind]
        tmp_mle = (sl_log.(bh.B[k_inds, zs[end]]) .+ logps[k_inds]) |>
                   Vector |> sfp_logsumexp

        if bh.if_reg
            # tmp_mle += compute_pseudo_lkhd(bh, d_ind, zs; if_λ=false)
            # tmp_mle += pseudo_lkhd(bh, d_ind, zs) |> first
            tmp_mle += regfunc(bh, d_ind, zs)
        end

        logmle += tmp_mle
    end
    logmle += log_prior(bh)
    logmle
end

@inline function log_mcle(bh::VBMCMCHierarchy)
    logmle = 0.
    tmp_mle = 0.
    @inbounds for d_ind = 1:bh.n
        zs = findall(bh.z_x[:, d_ind] .> 0)
        k_ind = findfirst(bh.x_k[d_ind, 1:bh.K] .> 0)

        tmp_mle = logp(bh.X[:, d_ind], bh.G, bh.θs[k_ind])
        tmp_mle += sl_log(bh.B[k_ind, zs[end]])
        @assert tmp_mle != -Inf
        #
        if bh.if_reg
            # tmp_mle += compute_pseudo_lkhd(bh, d_ind, zs; if_λ=false)
            # tmp_mle += pseudo_lkhd(bh, d_ind, zs) |> first
            tmp_mle += regfunc(bh, d_ind, zs)
        end

        # tmp_mle += path_prior(bh, d_ind, zs; aftercleaning=false)

        logmle += tmp_mle
    end
    logmle += log_prior(bh)
    # logmle += log_prior(bh; if_hier=true, if_β=true, if_θ=true)
    logmle
end

@inline function log_mcle_separate(bh::VBMCMCHierarchy)
    logmle = 0.
    tmp_mle = 0.
    p_lkhd = 0.
    @inbounds for d_ind = 1:bh.n
        zs = findall(bh.z_x[:, d_ind] .> 0)
        k_ind = findfirst(bh.x_k[d_ind, 1:bh.K] .> 0)

        tmp_mle = logp(bh.X[:, d_ind], bh.G, bh.θs[k_ind])
        tmp_mle += sl_log(bh.B[k_ind, zs[end]])
        @assert tmp_mle != -Inf
        #
        if bh.if_reg
            p_lkhd += regfunc(bh, d_ind, zs)
        end
        logmle += tmp_mle
    end
    logmle += log_prior(bh)
    # logmle += log_prior(bh; if_hier=true, if_β=true, if_θ=true)
    logmle, logmle+p_lkhd
end

function regfunc(bh::VBMCMCHierarchy, d_ind::Int, zs::NodeVec)
    z = last(zs)
    leaves = findall(isone, bh.leaves)

    log_lkhd = 0.
    tmp_max_discris = -Inf
    @inbounds for ℓ = 1:bh.L
        z = zs[ℓ+1]
        sibs = siblings(bh, z)

        if !isempty(sibs)
            discris = bh.ϵ0 .- vec(bh.X[:, d_ind]' *
                                   (bh.E[:, z] .- bh.E[:, sibs]))

            indices = findall(discris .== maximum(discris))
            index = sample(indices)

            max_discri = discris[index]
            if max_discri > tmp_max_discris
                tmp_max_discris = max_discri
            end
        end
    end

    loc = bh.C * tmp_max_discris
    # -max(0, 2loc) = min(0, -2loc)
    return min(0, -2 * loc)
end

@inline function update_λs!(bh::VBMCMCHierarchy, d_ind::Int, zs::NodeVec)
    @inbounds lz = bh.lzs[d_ind]
    @inbounds s = bh.ss[d_ind]
    @inbounds loc = bh.C *
        (bh.ϵ0 - bh.X[:, d_ind]' * (bh.E[:, lz] .- bh.E[:, s]))
    @inbounds bh.λs[d_ind] = GeneralizedInverseGaussian(0.5, 1, loc^2) |> crand
    @inbounds bh.λs[d_ind] += 1e-5
end

function component_log_pdfs!(bh::VBMCMCHierarchy, logps::DoubleVec,
                             k_inds::IntVec, d_ind::Int)
    x = bh.X[:, d_ind]
    @inbounds @simd for i = 1:length(k_inds)
        k_ind = k_inds[i]
        logps[i] = logp(x, bh.G, bh.θs[k_ind])
    end
end

function parent(bh::VBMCMCHierarchy, z::Node)::Node
    @assert z == 1 || nnz(bh.adjmat[z, :]) == 1
    z == 1 ? 0 : findfirst(bh.adjmat[z, :] .> 0)
end

function children(bh::VBMCMCHierarchy, z::Node)::NodeVec
    @assert z > 0
    findall(isone, bh.adjmat[1:bh.max_node, z])
end

function siblings(bh::VBMCMCHierarchy, z::Node)::NodeVec
    z_star = parent(bh, z)
    chdr = children(bh, z_star)
    chdr[chdr .!= z]
end

function sample_node_param_prior!(bh::VBMCMCHierarchy, z_star::Node, z::Node)
    tmp_β = Vector(bh.η .* bh.B[1:bh.K, z_star])
    nz_ks = findall(tmp_β .> TOL)
    bh.B[1:bh.K, z] .= 0

    tmp = @> tmp_β[nz_ks] Dirichlet rrand lnormalize(1)
    bh.B[nz_ks, z] .= tmp
end

function sample_η_prior!(bh::VBMCMCHierarchy, z::Node)
    @inbounds bh.E[:, z] .= rrand(MvNormal(bh.dim, bh.ν0))
end

@inline function pseudo_lkhd_n_assignments(bh::VBMCMCHierarchy,
                                           d_ind::Int, zs::NodeVec)
    z = last(zs)
    leaves = findall(isone, bh.leaves)

    log_lkhd = 0.
    tmp_lz = 0
    tmp_s = 0
    tmp_max_discris = -Inf
    tmps = []
    @inbounds for ℓ = 1:bh.L
        z = zs[ℓ+1]
        sibs = siblings(bh, z)

        if !isempty(sibs)
            discris = bh.ϵ0 .- vec(bh.X[:, d_ind]' *
                                   (bh.E[:, z] .- bh.E[:, sibs]))

            indices = findall(discris .== maximum(discris))
            index = sample(indices)

            max_discri = discris[index]
            push!(tmps, discris)
            if max_discri > tmp_max_discris
                s = sibs[index]
                tmp_max_discris, tmp_s, tmp_lz = max_discri, s, z
            end
        end
    end

    @assert tmp_s != 0 "$tmps $(bh.adjmat) $zs"

    λ = bh.λs[d_ind]
    loc = bh.C * tmp_max_discris
    log_lkhd -= 0.5 * ((loc + λ)^2 / λ + sl_log(λ))
    log_lkhd, tmp_s, tmp_lz
end

@inbounds @inline function compute_pseudo_lkhd(bh::VBMCMCHierarchy, d_ind::Int,
                                               zs::NodeVec; if_λ::Bool=false)
    log_lkhd = 0.

    lz = bh.lzs[d_ind]
    s = bh.ss[d_ind]
    λ = bh.λs[d_ind]

    loc = bh.C * (bh.ϵ0 .- bh.X[:, d_ind]' * (bh.E[:, lz] .- bh.E[:, s]))

    log_lkhd -= 0.5 * ((loc + λ)^2 / λ + sl_log(λ))

    if if_λ
        log_lkhd += dlogpdf(InverseGaussian(1/abs(loc), 1), 1/λ)
    end
    log_lkhd
end

@inline function update_η!(bh::VBMCMCHierarchy, z::Node, ℓ::Int,
                           μ_::DoubleVec, Σ_inv::DoubleMat)
    fill!(Σ_inv, 0)
    fill!(μ_, 0)
    x::DoubleVec = zeros(bh.dim)
    tmp::DoubleMat = zeros(bh.dim, bh.dim)
    s_ind = v_ind = 0

    C, C_sq = bh.C, bh.C ^ 2
    @inbounds v_inds = findall(bh.lzs .== z)
    @inbounds @simd for i = 1:length(v_inds)
        v_ind = v_inds[i]
        s = bh.ss[v_ind]
        λ = bh.λs[v_ind]
        x .= bh.X[:, v_ind]
        tmp .= x * x' ./ λ

        Σ_inv .+= tmp
        μ_ .+= C_sq * tmp' * bh.E[:, s] .+ (C_sq * bh.ϵ0 / λ + C) * x
    end

    @inbounds s_inds = findall(bh.ss .== z)
    @inbounds @simd for i = 1:length(s_inds)
        s_ind = s_inds[i]
        v = getindex(findall(bh.z_x[:, s_ind] .> 0), ℓ+1)
        λ = bh.λs[s_ind]
        x .= bh.X[:, s_ind]
        tmp .= x * x' ./ λ

        Σ_inv .+= tmp
        μ_ .+= C_sq * tmp' * bh.E[:, v] .- (C_sq * bh.ϵ0 / λ + C) * x
    end

    @inbounds if !isempty(s_inds) && !isempty(v_inds)
        # μ_ .*= C_sq
        Σ = inv(C_sq .* Σ_inv + I .* bh.ν0^2) |> Symmetric |> DoubleMat
        try
            bh.E[:, z] .= MvNormal(Σ * μ_, Σ) |> rrand |> vec
        catch err
            #@warn("Error: ", err)
            bh.E[:, z] .= MvNormal(bh.dim, bh.ν0) |> rrand |> vec
        end
    else
        bh.E[:, z] .= MvNormal(bh.dim, bh.ν0) |> rrand |> vec
    end
end

@inline function nCRP_node!(bh::VBMCMCHierarchy, zs::NodeVec, ℓ::Int)
    tmp_β::DoubleVec = zeros(bh.K)
    nCRP_node!(bh, zs, ℓ, tmp_β)
end

@inline function nCRP_node!(bh::VBMCMCHierarchy, zs::NodeVec,
                            ℓ::Int, tmp_β::DoubleVec)
    z = new_node(bh)
    bh.adjmat[z, zs[ℓ]] = 1
    zs[ℓ+1] = z

    counter = 0
    while true
        sample_node_param_prior!(bh, zs[ℓ], z)
        all((!isnan).(bh.B[1:bh.K, z])) && break
        counter += 1
        if counter >= 5
            gf = parent(bh, zs[ℓ])
            error("$(bh.K)
                   grand father $gf and its beta is $(bh.B[:, gf])
                   the farther $(zs[ℓ])'s beta is $(bh.B[:, zs[ℓ]])")
        end
    end

    bh.if_reg && sample_η_prior!(bh, z)
end
