function inference!(MHS::RegMHPathSolver; kwargs...)
    kwargs_dict = Dict(kwargs)
    conv = get(kwargs_dict, :conv, false)
    burnin = get(kwargs_dict, :burnin, 50)
    run_index = get(kwargs_dict, :run_index, 1)

    @match conv begin
        false   => _inference!(MHS, burnin)
        true    => _inference_conv!(MHS, burnin, run_index)
    end
    clean_sparse!(MHS.bh)
end

function inference!(MHS::MHPathSolver; kwargs...)
    kwargs_dict = Dict(kwargs)
    conv = get(kwargs_dict, :conv, false)
    burnin = get(kwargs_dict, :burnin, 50)
    run_index = get(kwargs_dict, :run_index, 1)

    @match conv begin
        false   => _inference!(MHS, burnin)
        true    => _inference_conv!(MHS, burnin, run_index)
    end
    clean_sparse!(MHS.bh)
end
