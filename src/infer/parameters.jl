abstract type AbstractBHierPar end

mutable struct NNBHierPar <: AbstractBHierPar
    G4α::Distribution
    G4γ::Distribution
    G4η::Distribution
    G4σ::Distribution

    # optional
    α::Float64
    γ::Float64
    η::Float64
    σ::Float64

    """Default choices will be set up"""
    NNBHierPar() = new(Gamma(3, 1), Gamma(4, 1), Uniform(0, 1.5), Uniform(0, 0.1))

    function NNBHierPar(G4α, G4γ, G4η, G4σ)
        this = new(G4α, G4γ, G4η, G4σ)
    end
end

function resample!(pars::NNBHierPar)
    pars.α = rrand(pars.G4α)
    pars.γ = rrand(pars.G4γ)
    pars.η = rrand(pars.G4η)
    pars.σ = rrand(pars.G4σ)
end

resample(pars::NNBHierPar) = let
    resample!(pars)
    Real[pars.α, pars.γ, pars.η, pars.σ]
end

function resample!(pars::NNBHierPar, bh::BHierarchy)
    bh.α = pars.α = rrand(pars.G4α)
    bh.γ = pars.γ = rrand(pars.G4γ)
    bh.η = pars.η = rrand(pars.G4η)
    bh.σ = pars.σ = rrand(pars.G4σ)
end

function assign!(pars::NNBHierPar, bh::BHierarchy)
    bh.α = pars.α
    bh.γ = pars.γ
    bh.η = pars.η
    assign_Σ!(bh.G, pars.σ^2 * PDMat(Matrix(I, bh.dim, bh.dim)))
end

collect_parameters(bh::BHierarchy, _::NNBHierPar) =
    [bh.α, bh.γ, bh.η, bh.G.Σ[1, 1]]


function collect_parameters!(pars::NNBHierPar, bh::BHierarchy)
    pars.α = bh.α
    pars.γ = bh.γ
    pars.η = bh.η
    pars.σ = bh.G.Σ[1, 1]
end


function pars_logpdf(bh::BHierarchy, bh_pars::NNBHierPar)
    pars_logpdf(collect_parameters(bh, bh_pars), bh_pars)
end

@inline function pars_logpdf(params::Vector, bh_pars::NNBHierPar)
    logp = 0

    α, γ0, η, σ = params
    logp += dlogpdf(bh_pars.G4α, α) + dlogpdf(bh_pars.G4γ, γ0)
    logp += dlogpdf(bh_pars.G4η, η) + dlogpdf(bh_pars.G4σ, σ)

    logp
end

function pars_logpdf(bh_pars::NNBHierPar)
    pars_logpdf([bh_pars.α, bh_pars.γ, bh_pars.η, bh_pars.σ],
                bh_pars)
end



mutable struct NIWBHierPar  <: AbstractBHierPar
    G4α::Distribution
    G4γ::Distribution
    G4η::Distribution
    G4λ::Distribution
    G4ν::Distribution
    dim::Int

    # optional
    α::Float64
    γ::Float64
    η::Float64
    λ::Float64
    ν::Float64

    NIWBHierPar() = new(Gamma(3, 1), Gamma(3, 1), Gamma(3, 1),
                        Beta(5, 5), Gamma(10, 2), 1, 0, 0, 0, 0, 0)

    NIWBHierPar(G4α, G4γ, G4η, G4λ, G4ν, dim) =
        new(G4α, G4γ, G4η, G4λ, G4ν, dim, 0, 0, 0, 0, 0)

    NIWBHierPar(dim) =
        new(Gamma(1, 1), Gamma(1, 1), Gamma(1, 1),
            Beta(0.25, 0.25), Gamma(10, 2), dim, 0, 0, 0, 0, 0)
end

function resample!(pars::NIWBHierPar)
    pars.α = rrand(pars.G4α)
    pars.γ = rrand(pars.G4γ)
    pars.η = rrand(pars.G4η)

    pars.λ = rrand(pars.G4λ)
    pars.ν = rrand(pars.G4ν) + pars.dim - 1
end

resample(pars::NIWBHierPar) = let
    resample!(pars)
    Real[pars.α, pars.γ, pars.η, pars.λ, pars.ν]
end

function resample!(pars::NIWBHierPar, bh::BHierarchy)
    bh.α = pars.α = rrand(pars.G4α)
    bh.γ = pars.γ = rrand(pars.G4γ)
    bh.η = pars.η = rrand(pars.G4η)

    bh.H.λ = pars.λ = rrand(pars.G4λ)
    bh.H.ν = pars.ν = rrand(pars.G4ν) + pars.dim - 1
end

function assign!(pars::NIWBHierPar, bh::BHierarchy)
    bh.α = pars.α
    bh.γ = pars.γ
    bh.η = pars.η

    bh.H.λ = pars.λ
    bh.H.ν = pars.ν
end

collect_parameters(bh::BHierarchy) =
    Real[bh.α, bh.γ, bh.η, bh.H.λ, bh.H.ν]

function collect_parameters!(pars::NIWBHierPar, bh::BHierarchy)
    pars.α = bh.α
    pars.γ = bh.γ
    pars.η = bh.η

    pars.λ = bh.H.λ
    pars.ν = bh.H.ν
end


function pars_logpdf(bh::BHierarchy, bh_pars::NIWBHierPar)
    pars_logpdf(collect_parameters(bh), bh_pars)
end

@inline function pars_logpdf(params::Vector, bh_pars::NIWBHierPar)
    logp = 0

    α, γ0, η, λ, ν = params
    logp += dlogpdf(bh_pars.G4α, α) + dlogpdf(bh_pars.G4γ, γ0)
    logp += dlogpdf(bh_pars.G4η, η) + dlogpdf(bh_pars.G4λ, λ)
    logp += dlogpdf(bh_pars.G4ν, ν-(bh_pars.dim-1))

    logp
end

function pars_logpdf(bh_pars::NIWBHierPar)
    pars_logpdf([bh_pars.α, bh_pars.γ, bh_pars.η, bh_pars.λ, bh_pars.ν],
                bh_pars)
end
