const SpMCSC = SparseMatrixCSC
const SpVec = SparseVector
const IArray2D = Vector{Vector{Int}}
const IDictArray = Vector{Dict{Int, Int}}
const DoubleVec = Vector{Float64}
const DoubleMat = Matrix{Float64}
const DoubleDMat = Diagonal{Float64}
const IntVec = Vector{Int}


# root path
const rpath = normpath(joinpath(dirname(@__FILE__), "../../"))
# output path
const opath = normpath(joinpath(dirname(@__FILE__), "../../output/"))
# data path
const dpath = normpath(joinpath(dirname(@__FILE__), "../../data/"))
# plots path
const plotpath = normpath(joinpath(dirname(@__FILE__), "../../plots/"))

const nastrings = String["NA", "na", "n/a", "missing"]

# the postfix for the JLD files that store the BHierarchy instance
const BHIER_POSTFIX = "_bhierarchy.jld"

const TOL = 1e-6

const PROB_TOL = 1e-6

const Node = Int

const NodeVec = Vector{Node}

const P_E = 0.9

# Abbrv. for the external functions

# For LinearAlgebra
# const lpinv = LinearAlgebra.pinv
const lnormalize = LinearAlgebra.normalize
const lnormalize! = LinearAlgebra.normalize!

# For SLEEF
const sl_exp = SLEEF.exp
const sl_log = SLEEF.log_fast
const sl_pow = SLEEF.pow

# For SpecialFunctions
const sp_lgamma = SpecialFunctions.loggamma

const PERIOD = 50

const VAR_PARTS = 8

function row_mul(A, v)
    r = similar(A)
    @inbounds @simd for j = 1:size(A, 2)
        for i = 1:size(A, 1)
            r[i, j] = v[j] * A[i, j]
        end
    end
    r
end

zcols(a::SpMCSC) = collect(i for i in 1:a.n if a.colptr[i] == a.colptr[i+1])

nzcols(a::SpMCSC) = collect(i for i in 1:a.n if a.colptr[i] != a.colptr[i+1])

nnzcols(a::SpMCSC) = sum(1 for i in 1:a.n if a.colptr[i] != a.colptr[i+1])

const DIM_THD_FOR_GPU = 10

@inline function sfp_logsumexp(w::Vector{T}) where T
    offset = maximum(w)
    s = zero(T)
    @inbounds @simd for i = 1:length(w)
        s += sl_exp(w[i]-offset)
    end
    sl_log(s) + offset
end
