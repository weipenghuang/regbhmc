function checkpath(path::String)
    isdir(path) == 0 && (@warn("Creating the path $path"); mkpath(path))
end

function savedf(df::DataFrame, fname::String;
                path::String=opath, header::Bool=true, delim="\t")
    checkpath(path)
    CSV.write(joinpath(path, fname), df; header=header, delim=delim)
end

loaddf(fname::String; path::String=dpath, delim="\t", header=false) =
    CSV.File(joinpath(dpath, fname); delim=delim, header=header) |> DataFrame

function savejson(data::String, fname::String; path::String=opath, args...)
    checkpath(path)
    endswith(fname, ".json") || (fname = string(fname, ".json"))
    open(joinpath(path, fname), "w") do file
        write(file, data)
    end
end

loadjson(fname::String; path::String=opath) = let
    data = open(joinpath(path, fname), "r") do file
        read(file, String)
    end
    JSON.parse(data)
end

function savegraph(g::AbstractGraph, fname::String;
                   path::String=opath, args...)
    checkpath(path)
    endswith(fname, ".jgz") || (fname = string(fname, ".jgz"))
    LightGraphs.savegraph(joinpath(path, fname), g)
end

loadgraph(fname::String; path::String=opath) =
    LightGraphs.loadgraph(joinpath(path, fname))

function savesparse(data::SparseMatrixCSC, fname::String;
                    path::String=opath, args...)
    savejld(data, fname, path, vname="sparse")
end

function loadsparse(fname::String; path::String=opath)::SparseMatrixCSC
    try
        data = loadjld(fname, path, "sparse")
    catch err
        rethrow(err)
    end
    data
end
