"""
Offset is usually the number of data entries.
It is used to fix the node index in the hierarchy.
Input data is a dictionary containing the target_names
and optionally the target entries
"""
function extract_tree_from_target(data::Dict, offset::Int=0; sep::String=".")
    !haskey(data, "target_names") &&
        throw("The input dictionary must contain
               the key 'target_names' and its corresponding value")
    @assert haskey(data, "target")

    offset == 0 && (offset = length(data["target"]))
    n = length(data["target_names"])
    count = 0

    tgt_dict_exiting::Dict{String, Int} =
        Dict(tgt => i for (i, tgt) in enumerate(data["target_names"]))

    tgt_dict = Dict{String, Int}()
    targets = []
    tmps = []
    for (i, tgt_name) in enumerate(data["target_names"])
        tmps = split(tgt_name, sep)
        push!(targets, tmps)
        for j in 1:length(tmps)-1
            key = join(tmps[1:j], sep)
            !haskey(tgt_dict, key) &&
                (count += 1; get!(tgt_dict, key, count))
        end
    end

    num_new_clust = length(tgt_dict)
    for (_, tgt_name) in enumerate(data["target_names"])
        get!(tgt_dict, tgt_name, tgt_dict_exiting[tgt_name]+num_new_clust)
    end

    tgt_size = length(tgt_dict)
    for (k, v) in tgt_dict
        tgt_dict[k] = offset + tgt_size - v + 1
    end

    root = offset + tgt_size + 1

    t = DiGraph(root)
    edges = []
    for (i, tgts) in enumerate(targets)
        source = root
        for j in 1:length(tgts)
            key = join(tgts[1:j], sep)
            sink = tgt_dict[key]
            !has_edge(t, source, sink) && @assert add_edge!(t, source, sink)

            source = sink
        end
    end

    for (i, tgt) in enumerate(data["target"])
        dest = offset + tgt_size - tgt - num_new_clust + 1
        @assert add_edge!(t, dest, i)
    end
    @assert is_connected(SimpleGraph(t))

    return t
end

@inline function data_handler(dataname::String)
    data = DoubleMat(undef, 0, 0)
    labels = Array{Int}(undef, 0)

    if dataname ∈ Set(["r15", "compound"])
        df = loaddf("$dataname/$dataname.csv", delim='\t')
        data = convert(Matrix, df[:, 1:end-1])
        labels = convert(Vector, df[end])

    elseif dataname ∈ Set(["glass", "redwine"])
        df = loaddf("$dataname/$dataname.csv", delim=',', header=true)
        data = convert(Matrix, df[:, 1:end-1])
        labels = convert(Vector, df[end])

    elseif dataname ∈ Set(["zoo", "wals"])
        df = loaddf("$dataname/$dataname.csv", delim=',', header=true)
        data = convert(Matrix, df[:, 2:end-1])
        labels = convert(Vector, df[end])

    elseif dataname ∈ Set(["animals"])
        df = loaddf("$dataname/$(dataname)_after_pca_7.csv", delim=',', header=true)
        data = convert(Matrix, df[:, 2:end])

    elseif dataname ∈ Set(["animal", "amazon"])
        df = loaddf("$dataname/$(dataname)_after_pca.csv", delim=',')
        data = convert(Matrix, df)

    elseif dataname == "amazon_new"
        df = loaddf("$dataname/$(dataname)_sampled_mat.csv", delim='\t', header=false)
        data = convert(Matrix, df)

    elseif dataname == "fmnist"
        df = loaddf("$dataname/sampled_$(dataname)2.csv", delim=',', header=true)
        data = convert(Matrix, df[:, 2:end-1])

    elseif dataname == "AAAI"
        df = loaddf("$dataname/$(dataname)_after_pca.csv", delim='\t',
                    header=false)
        data = convert(Matrix, df[:, :])

    elseif dataname ∈ Set(["syn_1000", "syn_10000", "syn_100000"])
        df = loaddf("synthetic/$(dataname).csv", delim='\t', header=true)
        data = convert(Matrix, df[:, :])

    elseif dataname == "mall"
        df = loaddf("mall/mall.csv", delim=',', header=true)
        data = Matrix(df[:, 4:5])

    elseif dataname == "mall_3d"
        df = loaddf("mall/mall.csv", delim=',', header=true)
        data = convert(Matrix, df[:, 3:5])

    else
        throw("The dataset '$dataname' is not yet supported")
    end

    @assert !isempty(data)

    return data, labels
end
