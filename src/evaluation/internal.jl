coherence(x_z::Vector{BitSet}, ϵ::Float64=1e-2) =
    sum(log((length(intersect(a, b))+ϵ)/length(a)) for a in x_z, b in x_z)
