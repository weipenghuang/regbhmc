entropy(x::Vector) = -sum(xlogx(p) for p in prop(freqtable(x)))

MI(x::Vector, y::Vector) = entropy(x) + entropy(y) - jointentropy(x, y)

jointentropy(x::Vector, y::Vector) =
    -sum(xlogx(p) for p in prop(freqtable(x, y)))

"""
- labels: the ground truth
- clusters: the prediction
"""
purity(labels::Vector, clusters::Vector)::Float64 =
    sum(maximum(freqtable(labels, clusters), dims=1)) / length(clusters)

inverse_purity(labels::Vector, clusters::Vector)::Float64 =
    purity(clusters, labels)

function Fmeasure(labels::Vector, clusters::Vector)::Float64
    cmat = convert(Matrix, freqtable(labels, clusters))

    n_labels = length(unique(labels))
    n_clusters = length(unique(clusters))

    pres = cmat ./ vec(sum(cmat, dims=1))'
    @inbounds @simd for i in 1:n_labels
        isnan(pres[i]) && (pres[i] = 0)
    end

    rec = cmat ./ vec(sum(cmat, dims=2))
    @inbounds @simd for i in 1:n_clusters
        isnan(rec[i]) && (rec[i] = 0)
    end

    fmeas::Vector{Float64} = zeros(n_labels)
    max_::Float64 = -Inf
    for i in 1:n_labels
        max = -Inf
        for j in 1:n_clusters
            val = 2 * rec[i, j] * pres[i, j] / (rec[i, j] + pres[i, j])
            val = isnan(val) ? 0 : val
            val > max_ && (max_ = val)
        end
        fmeas[i] = max_
    end

    label_nums = vec(sum(cmat, dims=2))[1:n_labels]
    fscore = fmeas' * label_nums / length(clusters)
    fscore
end

function Fmeasure2(labels::Vector, clusters::Vector, β::Real=1)::Float64
    cmat = convert(Matrix, freqtable(labels, clusters))

    n_labels = length(unique(labels))
    n_clusters = length(unique(clusters))

    P_row = sum(cmat, dims=1)
    P = sum(binomial.(P_row, 2))

    TP = 0
    @inbounds @simd for x in cmat
        x > 1 && (TP += binomial(x, 2))
    end
    prec = TP / P

    FN = 0
    @inbounds @simd for j = 1:n_clusters
        acc = 0
        total = P_row[j]
        for i = 1:n_labels
            if cmat[i, j] != 0
                acc += cmat[i, j]
                FN += cmat[i, j] * (total - acc)
            end
        end
    end
    rec = TP / (TP + FN)

    fscore = (β^2 + 1) * prec * rec / (β^2 * prec + rec)
    fscore
end

""" normalized mutual information """
NMI(labels::Vector, clusters::Vector)::Float64 =
    2 * MI(labels, clusters) / (entropy(labels) + entropy(clusters))

NMI_max(labels::Vector, clusters::Vector)::Float64 =
    MI(labels, clusters) / max(entropy(labels), entropy(clusters))

ARI(labels::Vector, clusters::Vector) = randindex(labels, clusters)[1]

hier_purity(label_mat::Matrix, cluster_mat::Matrix) =
    hier_metric_auxiliary(label_mat, cluster_mat, purity)

hier_ARI(label_mat::Matrix, cluster_mat::Matrix) =
    hier_metric_auxiliary(label_mat, cluster_mat, ARI)

hier_kendall(label_mat::Matrix, cluster_mat::Matrix) =
    hier_metric_auxiliary(label_mat, cluster_mat, corkendal)

hier_jaccard(label_mat::Matrix, cluster_mat::Matrix) =
    hier_metric_auxiliary(label_mat, cluster_mat, jaccard)

hier_metric_auxiliary(label_mat::Matrix, cluster_mat::Matrix, f::Function) = let
    @assert size(label_mat) == size(cluster_mat)
    n = size(label_mat, 2)
    [f(label_mat[:, i], cluster[:, i]) for i = 1:n]
end
