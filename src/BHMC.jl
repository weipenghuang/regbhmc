module BHMC

using Clustering
using CSV
using DataFrames: DataFrame
using DataStructures
using Distances
using Distributions
using FreqTables
using IterTools
using JSON
using Lazy
using LightGraphs
using LinearAlgebra
using Logging

using Match
using NPZ

#using PyCall
# using RCall
using Random
using PDMats

using SparseArrays
using SpecialFunctions
using Statistics
using StatsBase
using StatsFuns

using SLEEF

using TimerOutputs


export
    rpath,
    opath,
    dpath,
    plotpath,
    NodeVec
include("common/config.jl")

export
    checkpath,
    savejson,
    loadjson,
    savegraph,
    loadgraph,
    savesparse,
    loadsparse,
    savedata,
    loaddata,
    savedf,
    loaddf
include("common/utils.jl")

export
    extract_tree_from_target,
    data_handler
include("common/datahandler.jl")


export
    CMvNormal,
    CNIW,
    CNW,
    CMvBer,
    CMvBeta
include("random/config.jl")

export
    DistParameters
include("random/distparams.jl")

include("random/abstractdist.jl")
include("random/normalnormal.jl")
include("random/nwnormal.jl")
include("random/niwnormal.jl")
include("random/gem.jl")
include("random/discrete.jl")

export
    GeneralizedInverseGaussian,
    crand,
    cmean
include("random/gig.jl")

export
    Hierarchy,
    construct_tree!,
    Z_to_tree!,
    tree_to_Z,
    save_hier,
    Z_to_etetree,
    Z_to_etetree_v2,
    plot_graph,
    plot_phylotree,
    plot_etetree
include("hierarchy/hierarchy.jl")

export
    BHierarchy,
    initialize!,
    convert2hierarchy,
    convert2hierarchy!,
    save_bhier,
    load_bhier,
    get_labels
include("hierarchy/abstractbhierarchy.jl")
include("hierarchy/bhierarchy.jl")
include("hierarchy/bhutils.jl")
include("hierarchy/bhstats.jl")

export
    VBHierarchy,
    get_labels,
    semicopy!
include("hierarchy/vbh/vbhierarchy.jl")
include("hierarchy/vbh/vbhutils.jl")
include("hierarchy/vbh/vbhstats.jl")

export
    VBMCMCHierarchy,
    get_labels
include("hierarchy/vbh/vbmcmchierarchy.jl")

export
    log_post_v2,
    log_mcle_v2,
    log_mcle
include("random/ncrp.jl")

include("infer/parameters.jl")
include("infer/abstractsolver.jl")

export
    test!,
    infer!,
    get_clean_bh,
    MHPathSolver
include("infer/mhpath.jl")

export
    test!,
    infer!,
    # get_clean_bh,
    RegMHPathSolver,
    log_mle,
    log_mcle_separate
include("infer/regmhpath.jl")

export
    inference!
include("infer/inference.jl")

export
    average_empir_full_dist
include("random/distance.jl")

export
    bhmc_mh,
    bhmc_vi,
    bhmc_rvi,
    bhmc_rsvi,
    bhmc_slice,
    bhmc_regmh,
    sim_mh,
    sim_svi,
    sim_vi,
    sim_rsvi,
    sim_rvi,
    scores
include("simulation/sim.jl")

export
    sample_from_tree
include("simulation/sampledata.jl")

export
    purity,
    inverse_purity,
    NMI,
    NMI_max,
    ARI,
    hier_purity,
    hier_ARI,
    hier_kendall,
    hier_jaccard
include("evaluation/external.jl")

export
    coherence
include("evaluation/internal.jl")


end
