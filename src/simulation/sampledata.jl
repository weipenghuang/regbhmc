 @inline function sample_from_tree(n::Int, d::Int; kwargs...)
    kwargs_dict = Dict(kwargs)
    α::Float64 = get(kwargs_dict, :α, 5.)
    γ0::Float64 = get(kwargs_dict, :γ, 10.)
    γ::Float64 = get(kwargs_dict, :β, 2.)
    L::Int = get(kwargs_dict, :layer, 4)

    # μ0 = [0., 0.]
    # Σ0 = [50. 20.; 20. 50.]
    μ0::DoubleVec = zeros(d)
    Σ0 = Diagonal(ones(d)) |> Matrix |> PDMat

    # H = CMvNormal(μ0, Σ0)
    H = CNIW(μ0, Σ0, 0.01, 100)
    G = CMvNormal()

    X::DoubleMat = zeros(d, n)
    bh = BHierarchy(X, α, γ0, γ, G, H, L, false)

    rand!(bh)
    return bh
end
