@inline function bhmc_mh(X::Union{Matrix, SpMCSC}; kwargs...)::MHPathSolver
    kwargs_dict = Dict(kwargs)
    α::Float64 = get(kwargs_dict, :α, Inf)
    β::Float64 = get(kwargs_dict, :γ0, Inf)
    η::Float64 = get(kwargs_dict, :η, Inf)
    L::Int = get(kwargs_dict, :L, Inf)
    burnin::Int = get(kwargs_dict, :burnin, Inf)
    λ::Float64 = get(kwargs_dict, :λ, Inf)
    ν::Float64 = get(kwargs_dict, :ν, Inf)
    run_index::Int = get(kwargs_dict, :run_index, 1)

    dim = size(X, 2)

    μ0 = zeros(dim)
    Σ0 = DoubleMat(I, dim, dim) |> PDMat
    H = CMvNormal(μ0, Σ0)

    μ = zeros(dim)
    Σ = DoubleMat(I, dim, dim) |> PDMat
    G = CMvNormal(μ, Σ)

    conv = get(kwargs_dict, :conv, false)
    bh = BHierarchy(copy(X'), α, β, η, G, H, L)

    MHS = MHPathSolver(bh)
    inference!(MHS; burnin=burnin, conv=conv, run_index=run_index)

    clusters1 = get_labels(bh, 1)
    clusters2 = get_labels(bh, 2)

    num1 = @> clusters1 unique length
    num2 = @> clusters2 unique length
    @info "$num1 $num2"

    num1 != num2 && (return MHS)
    return MHS
end

@inline function bhmc_regmh(X::Union{Matrix, SpMCSC}; kwargs...)::RegMHPathSolver
    kwargs_dict = Dict(kwargs)
    α::Float64 = get(kwargs_dict, :α, Inf)
    γ0::Float64 = get(kwargs_dict, :γ0, Inf)
    η::Float64 = get(kwargs_dict, :η, Inf)
    L::Int = get(kwargs_dict, :L, Inf)
    burnin::Int = get(kwargs_dict, :burnin, Inf)
    λ::Float64 = get(kwargs_dict, :λ, Inf)
    ν::Float64 = get(kwargs_dict, :ν, Inf)
    ϵ0::Float64 = get(kwargs_dict, :ϵ0, Inf)
    ν0::Float64 = get(kwargs_dict, :ν0, Inf)
    C::Float64 = get(kwargs_dict, :C, Inf)
    run_index::Int = get(kwargs_dict, :run_index, 1)
    if_reg::Bool = get(kwargs_dict, :if_reg, false)
    scale_sig = get(kwargs_dict, :scale_sig, 1)

    dim = size(X, 2)

    μ0 = zeros(dim)
    Σ0 = DoubleMat(I, dim, dim) |> PDMat
    H = CMvNormal(μ0, Σ0)

    μ = zeros(dim)
    Σ = (DoubleMat(I, dim, dim) * scale_sig) |> PDMat
    G = CMvNormal(μ, Σ)

    conv = get(kwargs_dict, :conv, false)
    bh = VBMCMCHierarchy(copy(X'), α, γ0, η,
                         G, H, L, C, ϵ0, ν0;
                         if_reg=if_reg)

    S = RegMHPathSolver(bh)
    inference!(S; burnin=burnin, conv=conv, run_index=run_index)

    return S
end

"
The function for simulating the clustering using Metropolis-Hastings sampler
Variables:
- dataname: the name of the dataset
- index: the index of the rounds within the repeatitions; affects the save naming
- burnin: the maximum loops for the burnin
- if_blah_blah: the setting for if to do something
"
@inline function sim_generic(bh,
                             dataname::String,
                             method::String,
                             g_labels;
                             verbose=false,
                             run_index=1,
                             if_save_tree=false,
                             if_plot_tree=false,
                             if_compute_score=true,
                             kwargs...)
    data = bh.X

    seed = get(kwargs, :seed, Inf)

    hier = Hierarchy()
    if if_save_tree
        convert2hierarchy!(hier, bh)
        # make it compatible with our python library
        for i = 1:size(hier.Z, 1)
            hier.Z[i] .-= 1
        end
        save_hier(hier, "$(dataname)_bhmc_$(method)_$(run_index-1)";
                  path="./output/$(dataname)_bhmc_$(method)")
    end

    if if_plot_tree
        for i = 1:size(hier.Z, 1)
            hier.Z[i] .+= 1
        end
        plot_etetree(hier, "./graphs/$(dataname)_bhmc_$(method)_$(index-1)")
    end

    println(if_compute_score)
    if if_compute_score
        # mat = zeros(bh.L, size(data, 2))
        # for ℓ in 1:bh.L
        #     clusters = get_labels(bh, ℓ)
        #     mat[ℓ, :] .= clusters
        #     println(length(unique(clusters)), "  ", length(clusters))
        # end
        # checkpath("output/$(dataname)_tree/")
        # npzwrite("output/$(dataname)_tree/$(dataname)_$(method)_$seed.npy",
        #           mat)

        if !isempty(g_labels)
            for ℓ in 1:bh.L
                clusters = get_labels(bh, ℓ)
                scores(g_labels, clusters)
            end
        else
            @warn "The dataset is not supported for computing the scores"
        end
    end
end

@inline function sim_mh(dataname;
                        verbose=false,
                        run_index=1,
                        burnin=50,
                        if_save_tree=false,
                        if_plot_tree=false,
                        if_compute_score=true,
                        kwargs...)::MHPathSolver
    X::Matrix{Float64}, g_labels::Array{Int} = data_handler(dataname)
    @info "Finish loading data..."

    S = bhmc_mh(X; burnin=burnin, run_index=run_index, kwargs...)
    sim_generic(S.bh, dataname, "mh", g_labels;
                verbose=verbose, run_index=run_index,
                if_save_tree=if_save_tree, if_plot_tree=if_plot_tree,
                if_compute_score=if_compute_score)
    return S
end

function scores(g_labels::Array{Int}, clusters::Array{Int})
    p_score = purity(g_labels, clusters)
    ip_score = inverse_purity(g_labels, clusters)
    nmi_score = NMI(g_labels, clusters)
    ari_score = ARI(g_labels, clusters)
    f_score = Fmeasure(g_labels, clusters)
    f_score2 = Fmeasure2(g_labels, clusters)
    method = "bhmc"
    println("""
        $method
        The purity score is             $p_score
        The inverse purity score is     $ip_score
        The NMI is                      $nmi_score
        The adjusted rand index is      $ari_score
        The f measure is                $f_score
        The f measure is                $f_score2
    """)
end
