const GUMBEL = Gumbel(0, 1)
const EXP1 = Exponential(1)

const dlogpdf = Distributions.logpdf
const dlogcdf = Distributions.logcdf
const dcdf = Distributions.cdf

const rrand = Random.rand
const rrand! = Random.rand!

const EXP1 = Exponential(1)

@inbounds function clogpdf(D::Dirichlet, x::AbstractVector{<:Float64})::Float64
    lp::Float64 = sp_lgamma(sum(D.alpha)) - sum(sp_lgamma.(D.alpha)) 
    lp += sum(sl_log.(x) .* (D.alpha .- 1))
    lp
end
