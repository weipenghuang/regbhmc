"""
A [c]ustom Normal Inverse Wishart struct
"""
mutable struct CNIW <: AbstractDistribution
    μ0::DoubleVec
    Ψ::PDMat{Float64}
    λ::Float64
    ν::Float64
    dim::Int
    logdetΨ::Float64
    IW::InverseWishart

    function CNIW(μ0::DoubleVec,  Ψ::PDMat{Float64},  λ::Float64, ν::Float64)
        @assert size(μ0, 1) == size(Ψ, 1)
        @assert ν > size(Ψ, 1) - 1 "ν has to be greater than the dim of the data -1"

        this = new(μ0, Ψ, λ, ν)
        this.dim = size(Ψ, 1)
        this.logdetΨ = logdet(Ψ)
        this.IW = InverseWishart(ν, Ψ)
        this
    end
end

rand(G::CNIW) = let
    Σ = rrand(G.IW)
    μ = MvNormal(G.μ0, Σ/G.λ) |> rrand
    (μ, Σ)
end

pdf(G::CNIW, μ::DoubleVec, Σ::Union{DoubleMat, PDMat{Float64}}) =
    logpdf(G, μ, Σ) |> exp

logpdf(G::CNIW, μ::DoubleVec, Σ::Union{DoubleMat, PDMat{Float64}}) =
    dlogpdf(G.IW, Σ) + @> MvNormal(G.μ0, Σ/G.λ) dlogpdf(μ)

"""
# Batch process for the marginal likelihoods of each individual datum
- X: the observations
- G: the distribution for sampling x
- H: the prior distribution
"""
@inline function batch_log_mgnl_lkhds!(ps::DoubleVec, X::DoubleMat,
                                       G::CMvNormal, H::CNIW)
    x::DoubleVec = zeros(G.dim)

    fill!(ps, -Inf)
    d, n = size(X)

    λ, ν = H.λ + 1, H.ν + 1

    log_coef = - d/2 * logπ
    log_coef += logmvgamma(d, ν/2) - logmvgamma(d, H.ν/2)
    log_coef += d/2 * (sl_log(H.λ) - sl_log(λ))
    log_coef += H.ν/2 * logdet(H.Ψ)

    Ψ_coef = H.λ / (H.λ + 1)

    X_ = X .- H.μ0
    for i = 1:n
        x .= @views X_[:, i]
        Ψ = pdadd(Ψ_coef * (x * x'), H.Ψ)
        ps[i] = -logdet(Ψ)
    end

    @. ps = ps * ν/2 + log_coef
end

"""
Marginalized probability density of x given distribution G with the conjugate
prior H.
D is of the shape (dim x n) where `dim` is the dimensions and
`n` is the number of data points
"""
@inline function logpdf(x::DoubleVec, D::DoubleMat,
                        G::CMvNormal, H::CNIW)::Float64
    dim, n = size(D)

    λ = H.λ + n
    ν = H.ν + n
    d::Float64 = ν - dim + 1
    xbar = vec(mean(D, dims=2))

    μ0 = (H.λ * H.μ0 + n * xbar) / λ
    μ = xbar - H.μ0

    Ψ = n == 1 ? (@> H.IW rrand) : (@> D' cov)
    pdadd!(Ψ, H.Ψ)
    Ψ += (H.λ*n)/(H.λ+n) * μ*μ'
    Ψ /= (d * λ)
    Ψ = Matrix(Symmetric(Ψ))
    Ψ0 = PDMat(Ψ)

    convert(PDMat{Float64}, Ψ0)
    @> MvTDist(d, μ0, Ψ0) dlogpdf(x)
end

rand_param(H::CNIW) = NIWParameters(rand(H)...)

logp(x::DoubleVec, _::CMvNormal, niwp::NIWParameters) =
    @>> x dlogpdf(MvNormal(niwp.μ, niwp.Σ))

logp(X::DoubleMat, _::CMvNormal, niwp::NIWParameters) =
    isempty(X) ? 0 : @> MvNormal(niwp.μ, niwp.Σ) dlogpdf(X) sum

logp(niwp::NIWParameters, H::CNIW) = logpdf(H, niwp.μ, niwp.Σ)

function set_params!(T::CNIW, λ::Float64, ν::Float64, μ::DoubleVec, Ψ::DoubleMat)
    Ψ = Ψ |> Matrix |> Symmetric |> PDMat
    T.λ, T.ν, T.μ0, T.Ψ, T.logdetΨ = λ, ν, μ, Ψ, logdet(Ψ)
    T.IW = InverseWishart(ν, Ψ)
end

@fastmath @inline function coef_n_likelihood(X::DoubleMat, G::CMvNormal, T::CNIW)
    n = size(X, 2)
    invΨ = inv(T.Ψ)

    coef = T.dim * -(log2π + 1/T.λ - log(2)) + logdet(invΨ)
    coef += sum(digamma(T.ν + 1 - d)/2 for d = 1:T.dim)

    ps::DoubleVec = fill(coef, n)
    tmp = DoubleVec(undef, T.dim)
    @inbounds for i = 1:n
        tmp .= X[:, i] - T.μ0
        ps[i] -= T.ν * quad(invΨ, tmp)
    end
    ps .*= .5

    coef, ps
end

@fastmath @inline function estimate_params!(Ts::Vector{CNIW}, H::CNIW,
                                            P::SpMCSC, X::DoubleMat)
    K = length(Ts)

    Ns = sum(P[:, 1:K], dims=1) |> vec
    nz = Ns .> 1e-4
    nz_ks = Vector(1:K)[nz]

    Xbar::DoubleMat = zeros(H.dim, K)
    Xbar[:, nz_ks] = X * P[:, nz_ks] ./ Ns[nz_ks]'

    dim = H.dim
    Ψs = Vector{Matrix}(undef, K)
    S = DoubleMat(undef, dim, dim)

    ms = DoubleMat(undef, dim, K)
    νs = DoubleVec(undef, K)
    λs = DoubleVec(undef, K)

    u = DoubleVec(undef, dim)

    @inbounds for k in nz_ks
        T = Ts[k]
        S[:, :] = sum(
            P[i, k] * (X[:, i] - Xbar[:, k]) * (X[:, i] - Xbar[:, k])'
            for i in 1:size(X, 2)
        )
        λs[k] = Ns[k] + H.λ

        ms[:, k] .= (Xbar[:, k] .* Ns[k] .+ H.λ * H.μ0) / λs[k]
        νs[k] = Ns[k] + H.ν

        u .= Xbar[:, k] .- H.μ0
        Ψs[k] = H.Ψ + S + (H.λ * Ns[k] / λs[k]) * (u * u')
    end

    foreach(k -> set_params!(Ts[k], λs[k], νs[k], ms[:, k], Ψs[k]), nz_ks)

    Ns, Xbar
end

function set_params!(ξ::Real, T::CNIW,
                     λ::Float64, ν::Float64,
                     μ::DoubleVec, Ψ::DoubleMat)
    Ψ = Ψ |> Matrix |> Symmetric |> PDMat
    T.λ = (1 - ξ) * T.λ + ξ * λ
    T.ν = (1 - ξ) * T.ν + ξ * ν
    T.μ0 = (1 - ξ) * T.μ0 + ξ * μ
    T.Ψ = (1 - ξ) * T.Ψ + ξ * Ψ
    T.logdetΨ = logdet(Ψ)
    T.IW = InverseWishart(ν, Ψ)
end

entropy_W(T::CNIW) = entropy(T.IW)

@inline function update_params!(θs::Vector{NIWParameters}, Ts::Vector{CNIW})
    @assert length(θs) == length(Ts)
    d = first(Ts).dim
    @inbounds for i in eachindex(θs)
        θs[i].μ = Ts[i].μ0
        θs[i].Σ = Ts[i].Ψ / (Ts[i].ν - d)
    end
end
