@inline function L2dist(as::DoubleVec, bs::DoubleVec, θs::NIWParameters)
    @assert length(as) == length(bs) == length(θs)
    K = length(as) - 1

    res = []
    @inbounds for k = 1:K
        @simd for j = 1:K
            if as[k] != 0 && bs[k] != 0
                log_term = sl_log(as[k]) + sl_log(bs[k]) +
                           dlogpdf(MvNormal(Θs[k].μ, θs[j].Σ+Θs[k].Σ), Θs[j].μ)
                push!(res, log_term)
            end
        end
    end

    return sfp_logsumexp(res)
end

@inline function empirical_full_dist(X::DoubleMat, Y::DoubleMat)::Float64
    se = 0
    l_x = size(X, 2)
    l_y = size(Y, 2)

    (l_x == 0 || l_y == 0) && return 0.

    @inbounds @simd for k = 1:l_x
        for j = 1:l_y
            se += sum((X[:, k] .- Y[:, j]).^2)
        end
    end

    se /= l_x * l_y
    se
end

@inline function empirical_full_dist(X::DoubleMat)::Float64
    se = 0
    l_x = size(X, 2)

    l_x == 0 && (return 0)

    @inbounds for i = 1:l_x
        @simd for j = i+1:l_x
            se += sum((X[:, i] .- X[:, j]).^2)
        end
    end

    se /= l_x * (l_x - 1) / 2
    se
end

empirical_mean_dist(X::DoubleMat, Y::DoubleMat) =
    sum((mean(X, dims=2) .- mean(Y, dims=2)).^2)

@inline function average_empir_full_dist(bh::VBMCMCHierarchy)
    node_q = Queue{Node}()
    enqueue!(node_q, 1)

    avg_inner_d = 0.
    avg_outter_d = 0.
    num_nodes = 0.
    num_edges = 0.
    @inbounds while !isempty(node_q)
        z = dequeue!(node_q)
        bh.n_z_x[z] == 0 && continue

        if z != 1
            X = bh.X[:, findall(bh.z_x[z, :] .!= 0)]
            X_m = mean(X, dims=2)

            z_star = parent(bh, z)
            chds = findall(bh.adjmat[:, z_star] .> 0)
            @inbounds for chd in chds
                if chd > z && nnz(bh.z_x[chd, :]) > 0
                    Y = bh.X[:, findall(bh.z_x[chd, :] .> 0)]
                    Y_m = mean(Y, dims=2)
                    avg_outter_d += sum((X_m .- Y_m).^2)
                    num_edges += 1
                end
            end

            if size(X, 2) > 1
                avg_inner_d += empirical_full_dist(X)
                num_nodes += 1
            end
        end

        chds = findall(bh.adjmat[:, z] .> 0)
        foreach(chd -> enqueue!(node_q, chd), chds)
    end

    return avg_inner_d / num_nodes, avg_outter_d / num_edges
end
