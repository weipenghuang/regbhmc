abstract type DistParameters end

mutable struct NNParameters <: DistParameters
    μ::DoubleVec
end

mutable struct NIWParameters <: DistParameters
    μ::DoubleVec
    Σ::Union{PDMat{Float64}, DoubleMat, Diagonal}
end

mutable struct BBParameters <: DistParameters
    ps::DoubleVec
end

mutable struct NWParameters <: DistParameters
    μ::DoubleVec
    Σ::Union{PDMat{Float64}, DoubleMat}
end
