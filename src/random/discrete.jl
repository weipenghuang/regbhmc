rand_discrete(ps::DoubleVec, n::Int) = @> ps Categorical rrand(n)

rand_discrete(ps::DoubleVec) = @> ps rand_discrete(1) first

rand_log_discrete(logps::DoubleVec) =
    (logps .+ rrand(GUMBEL, length(logps))) |> argmax

function rand_log_discrete(logps::DoubleVec, items::Vector{T})::T where T
    @assert length(logps) == length(items)
    @>> logps rand_log_discrete getindex(items)
end
