"""
The Griffiths-Engen-McCloske (GEM) distribution
"""
mutable struct GEM <: AbstractDistribution
    a::Float64          # parameter a in the beta distribution
    b::Float64          # parameter b in the beta distribution
    ν::Float64          # the remaining length of the stick

    GEM(b::Real) = new(1, b, 1.)
    GEM(a::Real, b::Real) = new(a, b, 1.)
end

function rand(G::GEM)::Float64
    β = rrand(Beta(G.a, G.b))
    π_ = G.ν * β
    G.ν -= π_
    π_
end

function rand_with_proportions(G::GEM)::Tuple{Float64, Float64}
    β = rrand(Beta(G.a, G.b))
    π_ = G.ν * β
    G.ν -= π_
    π_, β
end

reset!(G::GEM) = (G.ν = 1)

stickbreaking(G::GEM) = @lazy rand(G) : stickbreaking(G)

take_n_stkbrk(n::Int, G::GEM) =
    @lazy n ≤ 1 ? G.ν : prepend(rand(G), take_n_stkbrk(n-1, G))

take_stkbrk_gt(tol::Real, G::GEM) =
    @lazy G.ν ≤ tol ? G.ν : prepend(rand(G), take_stkbrk_gt(tol, G))

function take_stkbrk_gt_with_proportions(tol::Real, G::GEM)
    πs, βs = [], []
    while G.ν > tol
        π_, β = rand_with_proportions(G)
        push!(πs, π_)
        push!(βs, β)
    end
    push!(πs, G.ν)
    πs, βs
end

@inline function GEM_rand!(arr::Union{DoubleVec, SpVec}, α::Real, u0=TOL)
    B = Beta(1, α)
    β = rrand(B)
    ν = 1 - β
    arr[1] = β

    i = 1
    n = length(arr)
    while ν > u0 && i < n - 1
        i += 1
        β = rrand(B)
        π_ = ν * β
        arr[i] = π_
        ν -= π_
    end
    arr[i+1] = ν
end

@inline function log_GEM_rand!(arr::Union{DoubleVec, SpVec}, α::Real, u0=TOL)
    B = Beta(1, α)
    β = rrand(B)
    ν = log(1 - β)
    arr[1] = log(β)
    logu0 = log(u0)

    i = 1
    n = length(arr)
    while ν > logu0 && i < n - 1
        i += 1
        γ0 = rrand(B)
        arr[i] = ν + log(β)
        ν += log(1 - β)
    end
    arr[i+1] = log(ν)
end
