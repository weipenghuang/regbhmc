"""
This should be called after the old path is removed from bh
or, the new path has not yet been added to bh
"""
function q(zs::NodeVec, bh::Union{BHierarchy, VBMCMCHierarchy})
    logp = 0.
    last_z = 1
    for z in zs[2:end]
        chds = findall(!iszero, bh.adjmat[:, last_z])

        denom = bh.n_z_x[z] == 0 ? bh.α : bh.n_z_x[z]
        num = @> bh.n_z_x[chds] Vector sum

        logp += sl_log(denom / (num + bh.α))
        last_z = z
    end

    return logp
end

function CRP_weights!(ws::DoubleVec, α::Real, n_z_x::Union{DoubleVec, SpVec})
    @assert length(ws) == length(n_z_x) + 1
    @inbounds ws[1:end-1] .= n_z_x
    @inbounds ws[end] = α
    normalize!(ws, 1)
end

@inline function nCRP_node!(bh::BHierarchy, zs::NodeVec,
                            ℓ::Int, tmp_β::DoubleVec)
    z = new_node(bh)
    bh.adjmat[z, zs[ℓ]] = 1
    zs[ℓ+1] = z

    tmp_β .= bh.η .* bh.B[1:bh.K, zs[ℓ]]
    mask = tmp_β .> TOL
    bh.B[:, z] .= 0

    bh.B[mask, z] = @> tmp_β[mask] Dirichlet rrand
end

@inline function nCRP_node!(bh::BHierarchy, z_star::Node)
    z = new_node(bh)
    bh.adjmat[z, z_star] = 1
    zs[ℓ+1] = z

    tmp_β = bh.η .* bh.B[1:bh.K, z_star]
    mask = tmp_β .> TOL
    bh.B[:, z] .= 0

    bh.B[mask, z] = @> tmp_β[mask] Dirichlet rrand
    return z
end

@inline function nCRP_path!(zs::NodeVec, zps::NodeVec,
                            bh::Union{BHierarchy, VBMCMCHierarchy}, idx::Node)
    ws::DoubleVec = Vector()

    z = 1
    zs[1] = z
    bh.n_z_x[z] += 1

    tmp_β = DoubleVec(undef, bh.K)
    for ℓ = 1:bh.L
        get_zps!(zps, bh.adjmat, z)
        if isempty(zps)
            nCRP_node!(bh, zs, ℓ, tmp_β)
            z = zs[ℓ+1]
        else
            resize!(ws, length(zps)+1)
            CRP_weights!(ws, bh.α, bh.n_z_x[zps])

            zind = @> ws Vector rand_discrete
            zind == length(ws) ?
                (nCRP_node!(bh, zs, ℓ, tmp_β); z = zs[ℓ+1]) :
                (z = zps[zind]; zs[ℓ+1] = z)
        end
        bh.n_z_x[z] += 1
    end
end

@inline function nCRP_infer!(zs::NodeVec, zps::NodeVec,
                             bh::Union{BHierarchy, VBMCMCHierarchy}, idx::Node)
    fill!(zs, 0)

    nCRP_path!(zs, zps, bh, idx)

    z = last(zs)

    logps = zeros(bh.K)
    logps[1:bh.K-1] = component_log_pdfs(bh.X[:, idx], bh)
    logps[end] = bh.lkhds[idx]
    logps += sl_log.(bh.B[1:bh.K, z])
    k4x = @> logps Vector rand_log_discrete

    # k_inds = findall(bh.B[1:bh.K-1, zs[end]] .> TOL)
    # tmp_logps = zeros(length(k_inds)+1)
    # component_log_pdfs!(bh, tmp_logps, k_inds, idx)
    # push!(k_inds, bh.K)
    # tmp_logps[end] = bh.lkhds[idx]
    # tmp_logps .+= sl_log.(bh.B[k_inds, z])
    # k_ind = @> tmp_logps Vector rand_log_discrete
    # k4x = k_inds[k_ind]

    Q = q(zs, bh)

    k4x == bh.K && new_component2(bh)

    bh.x_k[idx, k4x] = 1
    bh.n_k_x[k4x] += 1
    @. bh.z_x[zs, idx] = 1
    @. bh.n_z_k[zs, k4x] += 1

    Q
end

@inbounds @inline function nCRP!(zs::NodeVec, zps::NodeVec,
                                 bh::Union{BHierarchy, VBMCMCHierarchy},
                                 idx::Node)
    fill!(zs, 0)

    nCRP_path!(zs, zps, bh, idx)

    z = last(zs)
    k4x = @> bh.B[1:bh.K, z] Vector rand_discrete

    bh.x_k[idx, k4x] = 1
    bh.n_k_x[k4x] += 1
    bh.z_x[zs, idx] .= 1
    bh.n_z_k[zs, k4x] .+= 1
end

log_prior_crp(n_z_x::DoubleVec, α::Float64)::Float64 = let
    n = length(n_z_x)
    rs = sp_lgamma(α) - sp_lgamma(sum(n_z_x)+α) +
         n*sl_log(α) + sum(sp_lgamma.(n_z_x))
    rs
end

@inbounds log_prior_θs(θs::Vector{<:DistParameters},
                       H::AbstractDistribution) = let
    sum(logp(θs[i], H) for i in eachindex(θs))
end

@inline function log_prior(bh::Union{BHierarchy, VBMCMCHierarchy};
                           if_hier::Bool=true,
                           if_β::Bool=false, if_θ::Bool=false)
    logprior = 0.

    zps::NodeVec = Vector()
    n_zps_x::DoubleVec = Vector()

    node_q = Queue{Tuple{Node, Float64}}()
    enqueue!(node_q, (1, bh.α))

    if_β && (logprior += log_prior_global(bh))
    dropzeros!(bh.n_z_x)
    tmp_lp_b = 0.
    @inbounds while !isempty(node_q)
        z, α = dequeue!(node_q)
        zps = get_zps(bh.adjmat, z)
        isempty(zps) && continue

        n_zps_x = nonzeros(bh.n_z_x[zps])
        isempty(n_zps_x) && continue

        logprior = log_prior_crp(n_zps_x, bh.α)

        inds = findall(!iszero, bh.n_z_x[zps])

        if_β && (logprior += log_prior_βs(z, zps[inds], bh))
        for (_, zp) in enumerate(zps[inds])
            enqueue!(node_q, (zp, bh.α))
        end
    end
    if_θ && (logprior += log_prior_θs(bh.θs, bh.H))
    logprior
end

function log_prior_global(bh::Union{BHierarchy, VBMCMCHierarchy})
    mask = findall(x -> x .> TOL, bh.B[1:bh.K-1, 1])
    βs = bh.B[mask, 1] |> Vector
    beta = Beta(1, bh.γ)
    y = 0.

    # current length of the previous parts of the stick
    tmp_len = 1.
    for β_ in βs
        y += dlogpdf(beta, β_/tmp_len)
        tmp_len -= β_
    end
    y
end

log_prior_βs(z::Node, zps::NodeVec, bh::Union{BHierarchy, VBMCMCHierarchy}) =
    sum(log_prior_βs_helper(z, zp, bh) for zp in zps)

@inline function log_prior_βs_helper(z::Node, zp::Node,
                                     bh::Union{BHierarchy, VBMCMCHierarchy})
    @assert bh.n_z_x[zp] != 0

    inds = findall(bh.B[1:bh.K, zp] .> TOL)
    β_z::DoubleVec = @> inds length zeros
    # β for the node z in the sparse format
    β_z_sp = Vector(bh.B[1:bh.K, z]) .* bh.η

    e_ind, s_ind = 0, 1
    for i in eachindex(inds)
        e_ind = inds[i]
        β_z[i] = sum(β_z_sp[s_ind:e_ind])
        s_ind = e_ind + 1
    end
    β_z .+= TOL

    e_ind != bh.K && (β_z[end] += @> β_z_sp[e_ind+1:bh.K] sum)
    lgp = @> β_z Dirichlet clogpdf(bh.B[inds, zp])
    # lgp = @> β_z Dirichlet dlogpdf(Vector(bh.B[inds, zp]))
    @assert lgp != -Inf

    return lgp == Inf ? 0. : lgp
end

"""
The likelihood along the path
"""
@inline function log_lkhd_path(d_ind::Node, zs::NodeVec,
                               bh::Union{VBMCMCHierarchy, BHierarchy})::Float64
    z = last(zs)
    x = bh.X[:, d_ind]
    k_inds = findall(!iszero, bh.B[1:bh.K-1, z])

    lkhds = (length(k_inds) + 1) |> zeros
    lkhds[1:end-1] .= map(θ -> logp(x, bh.G, θ), bh.θs[k_inds])
    lkhds[end] = bh.lkhds[d_ind]

    push!(k_inds, bh.K)
    y = (lkhds .+ sl_log.(@> bh.B[k_inds, z] Vector)) |> sfp_logsumexp
    y
end


"""
The likelihood along the path
"""
@inline function log_lkhd_path(d_ind::Node,
                               bh::Union{VBMCMCHierarchy, BHierarchy})::Float64
    zs = findall(bh.z_x[:, d_ind] .> 0)
    log_lkhd_path(d_ind, zs, bh)
end

log_lkhd(bh::Union{VBMCMCHierarchy, BHierarchy}) = sum(log_lkhd_path(i, bh) for i = 1:bh.n)

log_posterior(bh::Union{VBMCMCHierarchy, BHierarchy}; if_hier=true, if_β=false, if_θ=false) =
    log_prior(bh; if_hier=if_hier, if_β=if_β, if_θ=if_θ) + log_lkhd(bh)

log_mcle(bh::BHierarchy; if_hier=true, if_β=false, if_θ=false) =
    log_prior(bh; if_hier=if_hier, if_β=if_β, if_θ=if_θ) + log_lkhd(bh)


#####################################################################
####  This is for version 2
#####################################################################

log_lkhd_single(idx::Int, bh::BHierarchy)::Float64 = let
    z, k = leaf_4_ind(idx, bh), comp_4_ind(idx, bh)
    k == bh.K ?
        bh.lkhds[end] :
        logp(bh.X[:, idx], bh.G, bh.θs[k]) + sl_log(bh.B[k, z])
end

log_lkhd_v2(bh::Union{VBMCMCHierarchy, BHierarchy}) = sum(log_lkhd_single(i, bh) for i = 1:bh.n)
"""
Log posterior version 2
"""
log_post_v2(bh::Union{VBMCMCHierarchy, BHierarchy}; if_hier=true, if_β=true, if_θ=true) =
    log_prior(bh; if_hier=if_hier, if_β=if_β, if_θ=if_θ) + log_lkhd_v2(bh)

log_mcle_v2(bh::Union{VBMCMCHierarchy, BHierarchy}; if_hier=true, if_β=false, if_θ=false) =
    log_prior(bh; if_hier=if_hier, if_β=if_β, if_θ=if_θ) + log_lkhd_v2(bh)
