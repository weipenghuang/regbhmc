sample_slice_log(x::Real) = x - rrand(EXP1)

sample_slice(x::Real) = Uniform(0, x) |> rrand
