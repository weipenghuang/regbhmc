"""
A [c]ustom multivariate normal distribution struct
"""
mutable struct CMvNormal <: AbstractDistribution
    μ::DoubleVec
    Σ::Union{PDMat{Float64}, DoubleDMat, Diagonal}
    Σinv::Union{PDMat{Float64}, DoubleDMat, Diagonal}
    logdetΣ::Float64
    Σinvμ::DoubleVec
    dim::Int

    CMvNormal() = new()

    function CMvNormal(μ::DoubleVec,
                       Σ::Union{PDMat{Float64}, DoubleMat, Diagonal})
        @assert size(μ, 1) == size(Σ, 1)
        this = new(μ, Σ)

        this.dim = size(Σ, 1)
        this.Σinv = inv(Σ)
        this.logdetΣ = logdet(Σ)
        this.Σinvμ = this.Σinv * μ

        this
    end
end

"""
# Batch process for the marginal likelihoods of each individual datum
- G: the distribution for sampling x
- H: the prior distribution
- X: the observations
"""
function batch_mgnl_lkhds!(ps::DoubleVec, G::CMvNormal,
                           H::CMvNormal, X::DoubleMat)
    batch_log_mgnl_lkhs!(ps, G, H, X)
    ps .= exp.(ps)
end

"""
# Batch process for the log marginal likelihoods of each individual datum
- G: the distribution for sampling x
- H: the prior distribution
- X: the observations
"""
@inline function batch_log_mgnl_lkhds!(ps::DoubleVec, X::DoubleMat,
                                       G::CMvNormal, H::CMvNormal)
    dim = G.dim
    μ, Σ, Σinv = G.μ, G.Σ, G.Σinv
    logdetΣ = G.logdetΣ

    μ0, Σ0, Σ0inv = H.μ, H.Σ, H.Σinv
    logΣ0det = H.logdetΣ

    Cinv = Σinv + Σ0inv
    C = inv(Cinv)
    v_part = H.Σinvμ # Σ0inv * μ0

    logcoef = .5 * (logdet(C) - dim*log2π - logdetΣ - logΣ0det)
    μ0ᵀΣ0μ0 = μ0' * v_part

    vᵀCv, xᵀΣx = 0., 0.
    x::DoubleVec = zeros(dim)

    @inbounds @simd for i = 1:size(X, 2)
        x .= view(X, :, i)
        v = Σinv * x + v_part
        vᵀCv = v' * C * v
        xᵀΣx = x' * Σinv * x
#         vᵀCv = quad(C, v)
#         xᵀΣx = quad(Σinv, x)
        ps[i] = logcoef - .5 * (xᵀΣx + μ0ᵀΣ0μ0 - vᵀCv)
    end
end

pdf(x::DoubleVec, D::DoubleMat, G::CMvNormal, H::CMvNormal) =
    logpdf(x, D, G, H) |> exp

logpdf(x::DoubleVec, D::DoubleMat, G::CMvNormal, H::CMvNormal) = let
    n = size(D, 2)
    xbar = mean(D, dims=2)
    Σ = inv(H.Σinv + n*G.Σinv)
    μ = Σ * (H.Σinvμ + G.Σinv * xbar .* n)
    @> MvNormal(μ, Σ+G.Σ) dlogpdf(x)
end

rand(G::CMvNormal, n::Int=1) = @> MvNormal(G.μ, G.Σ) rrand(n)

rand_param(H::CMvNormal) = @>> MvNormal(H.μ, H.Σ) rrand NNParameters

logp(x::Vector, G::CMvNormal, params::NNParameters) =
    @>> x dlogpdf(MvNormal(params.μ, G.Σ))

logp(X::Matrix, G::CMvNormal, params::NNParameters) =
    isempty(X) ? 0 : @>> X dlogpdf(MvNormal(params.μ, G.Σ)) sum

logp(nnp::NNParameters, H::CMvNormal) = @> MvNormal(H.μ, H.Σ) dlogpdf(nnp.μ)

rand(G::CMvNormal, θ::NIWParameters, n::Int=1) = rrand(MvNormal(θ.μ, θ.Σ), n)

rand(G::CMvNormal, θ::NNParameters, n::Int=1) = rrand(MvNormal(θ.μ, G.Σ), n)


@inline function set_params_grad_asc!(ξ::Float64, T::CMvNormal, μ::DoubleVec,
                                      Σinv::Union{PDMat{Float64}, DoubleMat})
    # @inbounds T.μ .+= @views ξ * μ + sqrt(ξ) / 100 * rrand(NOISE)
    @inbounds T.μ .+= @views ξ * μ
    @inbounds T.Σ .+= @views ξ * Σinv
    @inbounds T.logdetΣ = logdet(T.Σ)
    @inbounds T.Σinv .= pinv(T.Σ)
    @inbounds T.Σinvμ .= @views T.Σinv * T.μ
end

function set_params_grad_asc!(ξ::Float64, T::CMvNormal, μ::DoubleVec)
    # NOISE = MvNormal(zeros(T.dim), Diagonal(ones(T.dim)))
    # @inbounds T.μ .+= @views ξ * (μ + .001 * rrand(NOISE))
    @inbounds T.μ .+= @views ξ * T.Σ * μ
    @inbounds T.Σinvμ .= @views T.Σinv * T.μ
end

@inline function set_μ!(H::CMvNormal, μ::DoubleVec)
    @inbounds H.μ .= μ
    @inbounds H.Σinvμ .= @views H.Σinv * μ
end
