"""
A [c]ustom Normal Wishart struct
"""
mutable struct CNW <: AbstractDistribution
    ξ::Float64
    ν::Real
    m::DoubleVec
    W::DoubleMat
    invW::DoubleMat
    dim::Int
    Wis::Wishart
    logdet_invW::Float64

    function CNW(m::DoubleVec, invW::DoubleMat, ξ::Real, ν::Real)
        @assert size(m, 1) == size(invW, 1) == size(invW, 2)
        @assert ν > size(invW, 1) - 1 "ν has to be greater than the dim of the data -1"

        invW = @> invW Symmetric Matrix
        this = new(ξ, ν, m, invW)
        this.dim = size(invW, 1)
        this.W = pinv(invW)
        this.Wis = Wishart(ν, invW)
        this.logdet_invW = logdet(invW)
        this
    end
end

function set_params!(T::CNW, ξ::Float64, ν::Float64,
                     m::DoubleVec, invW::DoubleMat)
    T.ξ, T.ν, T.m, T.invW, T.logdet_invW = ξ, ν, m, invW, logdet(invW)
    T.W = pinv(invW)
end

@inline function coef_n_likelihood(X::DoubleMat, G::CMvNormal, T::CNW, k)
    n = size(X, 2)
    coef = - (log(2) + 1/T.ξ) * T.dim + T.logdet_invW
    coef += sum(T.ν + 1 - d |> digamma for d = 1:T.dim)

    ps::DoubleVec = fill(coef, n)

    tmp = DoubleVec(undef, T.dim)
    @inbounds for i = 1:n
        tmp[:] = X[:, i] - T.m
        ps[i] -= T.ν * (@> T.W Symmetric PDMat quad(tmp))
    end
    ps .*= .5

    coef, ps
end

@inline function update_params!(θs::Vector{NWParameters}, Ts::Vector{CNW})
    d = first(Ts).dim
    @inbounds for i in eachindex(θs)
        θs[i].μ = Ts[i].m
        θs[i].Σ = Ts[i].invW / (Ts[i].ν - d) |> pinv
    end
end

@inbounds @inline function estimate_params!(Ts::Vector{CNW}, H::CNW,
                                            P::SpMCSC, X::DoubleMat)
    K = length(Ts)
    dim = first(Ts).dim

    Ns = sum(P[:, 1:K], dims=1) |> vec
    nz = Ns .> 1e-4
    nz_ks = Vector(1:K)[nz]
    println("NS   $nz_ks ", Ns[nz_ks])

    Xbar::DoubleMat = zeros(dim, K)
    Xbar[:, nz_ks] = X * P[:, nz_ks] ./ Ns[nz_ks]'
    println("X bar ", Xbar[:, nz_ks])

    invW = Vector{Matrix}(undef, K)
    S = DoubleMat(undef, dim, dim)

    ms = DoubleMat(undef, dim, K)
    ξs = DoubleVec(undef, K)
    νs = DoubleVec(undef, K)

    u = DoubleVec(undef, dim)
    @inbounds for k in nz_ks
        T = Ts[k]

        S[:, :] = sum(
            P[i, k] * (X[:, i] - Xbar[:, k]) * (X[:, i] - Xbar[:, k])'
            for i in 1:size(X, 2)
        )

        ξs[k] = Ns[k] + H.ξ
        # println("
        #     checking $k
        #     $(Xbar[:, k] .* Ns[k])
        #     $(H.ξ * H.m)
        #     $(Xbar[:, k] .* Ns[k] .+ H.ξ * H.m) $(ξs[k])
        # ")
        ms[:, k] = (Xbar[:, k] .* Ns[k] .+ H.ξ * H.m) / ξs[k]
        νs[k] = Ns[k] + (H.ν + 1)

        u[:] = Xbar[:, k] .- H.m
        invW[k] = H.invW + S + (H.ξ * Ns[k]) / ξs[k] * u * u'
        println("k is $k ====")
        println(ms[:, k], "  ", T.m)
        println(pinv(invW[k]), "   ", T.invW, " =====")
    end

    foreach(k -> set_params!(Ts[k], ξs[k], νs[k], ms[:, k], invW[k]), nz_ks)

    Ns, Xbar
end

entropy_W(T::CNW) = Wishart(T.ν, T.W) |> entropy

rand(G::CNW) = let
    Σ = rrand(G.Wis)
    μ = MvNormal(G.m, Symmetric(lpinv(Σ)/G.ξ)) |> rrand
    (μ, lpinv(Σ))
end

rand_param(H::CNW) = NWParameters(rand(H)...)
