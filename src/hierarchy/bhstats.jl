"""
Clean the sufficient statistics for the i-th data point x
"""
@inline function clean_ss!(bh::BHierarchy, k4x::Int, idx::Int)
    zs = get_zps(bh.z_x, idx)
    clean_ss!(bh, idx, k4x, zs)
end

@inbounds function clean_ss!(bh::BHierarchy, idx::Int, k4x::Int, zs::NodeVec)
    bh.n_k_x[k4x] -= 1
    bh.n_z_x[zs] .-= 1
    bh.n_z_k[zs, k4x] .-= 1

    bh.x_k[idx, k4x] = 0
    bh.z_x[zs, idx] .= 0

    clean_sparse!(bh)
end

@inbounds function add_ss!(bh::BHierarchy, idx::Int, k4x::Int, zs::NodeVec)
    bh.n_k_x[k4x] += 1
    bh.n_z_x[zs] .+= 1
    bh.n_z_k[zs, k4x] .+= 1

    bh.x_k[idx, k4x] = 1
    bh.z_x[zs, idx] .= 1
end

@inline function clean_sparse!(bh::BHierarchy)
    dropzeros!(bh.adjmat)
    dropzeros!(bh.n_z_x)
    dropzeros!(bh.n_k_x)
    dropzeros!(bh.n_z_k)
    dropzeros!(bh.z_x)
    dropzeros!(bh.x_k)
    dropzeros!(bh.B)
end

# "Get z primes"
# function get_zps(mat::SpMCSC, parent::Node, axis::Int=2)::Vector{Int}
#     @assert axis == 1 || axis == 2
#     axis == 2 ? mat[:, parent].nzind : mat[parent, :].nzind
# end

function get_zps(mat::SpMCSC, parent::Node, axis::Int=2)::Vector{Int}
    @assert axis == 1 || axis == 2
    axis == 2 ?
        findall(!iszero, mat[:, parent]) :
        findall(!iszero, mat[parent, :])
end

# @inline function get_zps!(nodes::Vector{Node}, mat::SpMCSC,
#                           parent::Node, axis::Int=2)
#     if axis == 2
#         resize!(nodes, length(mat[:, parent].nzind))
#         copyto!(nodes, mat[:, parent].nzind)
#     elseif axis == 1
#         resize!(nodes, length(mat[parent, :].nzind))
#         copyto!(nodes, mat[parent, :].nzind)
#     else
#         throw("axis cannot exceed 2")
#     end
# end

@inline function get_zps!(nodes::Vector{Node}, mat::SpMCSC,
                          parent::Node, axis::Int=2)
    inds = @match axis begin
        2 => findall(mat[:, parent] .> 0)
        1 => findall(mat[parent, :] .> 0)
        _ => throw("axis cannot exceed 2")
    end
    resize!(nodes, length(inds))
    nodes .= inds
end

"""
Copy the changing variables for the object `bh`
Some variables e.g., `n` and `likelihoods` are not changing unless
the data is changed.
"""
function semicopy!(bh::BHierarchy, bhp::BHierarchy)
    bh.K = bhp.K
    bh.max_node = bhp.max_node

    bh.n_k_x = copy(bhp.n_k_x)
    bh.n_z_x = copy(bhp.n_z_x)
    bh.n_z_k = copy(bhp.n_z_k)
    bh.x_k = copy(bhp.x_k)
    bh.z_x = copy(bhp.z_x)
    bh.B = copy(bhp.B)

    bh.adjmat = copy(bhp.adjmat)

    resize!(bh.θs, bhp.K-1)
    bh.θs .= bhp.θs
end


@inbounds function semi_clean!(bh::BHierarchy)
    bh.n_k_x .= 0
    bh.n_z_x .= 0
    bh.n_z_k .= 0
    bh.x_k .= 0
    bh.z_x .= 0
    bh.adjmat .= 0
    bh.B .= 0

    clean_sparse!(bh)

    bh.max_node = 0
    bh.K = 0
end

function clean!(A::SpMCSC)
    A .= 0
    dropzeros!(A)
end
