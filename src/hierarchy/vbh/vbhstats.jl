function ELBO(bh::VBHierarchy)
    elbo = log_posterior(bh) - log_q_posterior(bh)
    return elbo
end

ELBO_reg(bh::VBHierarchy, J, Z_mat) = J * log_regulization(bh, J, Z_mat)

" Posterior of the model "
@inline function log_posterior(bh::VBHierarchy)
    logp = 0.

    dim = bh.dim
    n_zs = nz_nodes_v1(bh)

    # for x given assignment and Parameters
    logp += - dim * log2π - dim/2
    for (k, T) in enumerate(bh.Ts)
        logp += -.5 * T.logdetΣ + bh.G.logdetΣ - .5 *
            sum(bh.P[k, n] * (bh.X[:, n] - T.μ)' * T.Σinv * (bh.X[:, n] - T.μ)
            for n in 1:bh.n
        )
    end
    # @info("acc ELBO for X is $logp")

    # for c cluster assignment
    leaf_inds = findall(!iszero, bh.leaves)
    for (i, leaf) in enumerate(leaf_inds)
        nz_ks = valid_var_k_inds(leaf, bh)

        ω = digamma.(bh.Ω[nz_ks, leaf]) .-
            digamma.(sum(bh.Ω[nz_ks, leaf], dims=1))
        ns = findall(!iszero, bh.Λ[:, leaf])
        logp += sum(bh.P[ns, nz_ks] * ω)
    end
    # @info("acc ELBO for C is $logp")

    # for V
    logp += compute_V(bh)
    # @info("acc ELBO for V is $logp")

    # for U
    logp += (bh.α - 1 ) * sum(digamma.(bh.A[n_zs, 1]) -
                              digamma.(bh.A[n_zs, 1] .+ bh.A[n_zs, 2])) -
            length(n_zs) * logbeta(1., bh.α)
    # @info("acc ELBO for U is $logp")

    # for O
    logp += (bh.γ - 1) * sum(digamma.(bh.E[1:bh.K+1, 1]) .-
                             digamma.(bh.E[1:bh.K+1, 1] .+
                                      bh.E[1:bh.K+1, 2])) -
            (bh.K+1) * logbeta(1, bh.γ)
    # @info("acc ELBO for O is $logp")

    # for B
    logη = log(bh.η)
    logp += sum(
        begin
            zp = parent(z, bh)
            nz_zp_ks = valid_var_k_inds(zp, bh)
            nz_ks = valid_var_k_inds(z, bh)

            dgs = digamma.(bh.Ω[nz_ks, z]) .- digamma.(sum(bh.Ω[nz_ks, z]))
            # nz_ks rather than nz_zp_ks is correct in next line
            # given some simplification
            out = dot(dgs, (lnormalize(bh.Ω[nz_ks, zp]) * bh.η .- 1)) +
                (length(nz_zp_ks) - 1) * logη +
                    sum(digamma.(bh.Ω[nz_zp_ks, zp]) .-
                        digamma.(sum(bh.Ω[nz_zp_ks, zp])))
            # @info(z,
            #     bh.Ω[nz_ks, z],
            #     bh.Ω[nz_ks, zp],
            #     bh.Ω[nz_zp_ks, zp],
            #     dgs,
            #     dot(dgs, (lnormalize(bh.Ω[nz_ks, zp]) * bh.η .- 1)),
            #     (length(nz_zp_ks) - 1) * logη,
            #     digamma.(bh.Ω[nz_zp_ks, zp]),
            #     digamma.(sum(bh.Ω[nz_zp_ks, zp])),
            #     sum(digamma.(bh.Ω[nz_zp_ks, zp]) .-
            #         digamma.(sum(bh.Ω[nz_zp_ks, zp]))),
            #     out
            # )
            out
        end
        for z in n_zs[2:end]
    )
    # @info("acc ELBO for B is $logp")

    # for phi
    logp += - dim*bh.K/2*log2π - bh.K/2*bh.H.logdetΣ - 1/2 *
        sum((T.μ - bh.H.μ)' * bh.H.Σinv * (T.μ - bh.H.μ) for T in bh.Ts)

    # @info("acc ELBO for phi is $logp")
    logp
end

" Posterior of the variational parameters "
@inline function log_q_posterior(bh::VBHierarchy)
    logq = .0
    n_zs = nz_nodes_v1(bh)

    # for C
    logq += sum(bh.P[bh.P .> 0] .* log.(bh.P[bh.P .> 0]))
    # @info("acc q ELBO for C is $logq")

    # for V ===> constant

    # for U
    inds = findall(!iszero, bh.U)
    logq += (bh.α - 1) * sum(
        digamma.(bh.A[inds, 1]) - digamma.(bh.A[inds, 1] .+ bh.A[inds, 2])
    )
    # @info("acc q ELBO for U is $logq")

    # for E
    dgms = sum(bh.E)
    logq += (bh.γ - 1) * sum(digamma.(bh.E[1:bh.K+1]) .- digamma.(dgms))
    # @info("acc q ELBO for E is $logq")

    # for B
    logq += sum(
        begin
            nz_ks = valid_var_k_inds(z, bh)
            #
            # @info(bh.Ω[nz_ks, z],
            #     bh.Ω[nz_ks, z] .- 1,
            #     digamma.(bh.Ω[nz_ks, z]) .- digamma(sum(bh.Ω[nz_ks, z]))
            # )

            if !isempty(nz_ks)
                dot(bh.Ω[nz_ks, z] .- 1,
                    digamma.(bh.Ω[nz_ks, z]) .- digamma(sum(bh.Ω[nz_ks, z])))
            else
                0
            end
        end
        for z in n_zs[2:end]
    )
    # @info("tmp elbo q is $logq")
    logq += - sum(
        begin
            nz_ks = valid_var_k_inds(z, bh)
            !isempty(nz_ks) ? logbeta_vec(bh.Ω[nz_ks, z]) : 0
        end
        for z in n_zs[2:end]
    )
    # @info("acc q ELBO for B is $logq")

    # for ϕ
    logq += - bh.dim*bh.K/2 * (log2π+1)-1/2 * sum(bh.Ts[k].logdetΣ for k=1:bh.K)
    # @info("acc q ELBO for phi is $logq")

    logq
end


" Regularization "
@inline function log_regulization(bh::VBHierarchy, J::Float64, Z_mat::SpMCSC)
    logr = .0

    lkhds::SpMCSC{Float64} = spzeros(bh.K, bh.n)
    d_inds::IntVec = Vector(1:bh.n)

    l_expected_lkhds!(lkhds, bh, d_inds)

    reg_terms::SpMCSC{Float64} = spzeros(bh.K, bh.n)
    A::SpMCSC{Float64} = spzeros(bh.K, bh.n)
    nz_ks = IntVec()

    @inbounds for leaf in get_leaf_inds(bh)
        nz_reg_zs = findall(!iszero, Z_mat[:, leaf])
        isempty(nz_reg_zs) && continue

        inds = findall(!iszero, bh.z_x[leaf, d_inds])
        isempty(inds) && continue

        inds = d_inds[inds]
        for zp in nz_reg_zs
            clean!(A)
            nz_ks = valid_var_k_inds(zp, bh)
            A[nz_ks, inds] .= @views lkhds[nz_ks, inds] .+
                log.(bh.Ω[nz_ks, zp])

            reg_terms[nz_ks, inds] .+= @views A[nz_ks, inds]
        end
    end

    logr = sum(reg_terms)
    logr
end

get_leaf_inds(bh::VBHierarchy) = findall(!iszero, bh.leaves)

" Find the valid components and return the indices "
valid_k_inds(z::Node, bh::VBHierarchy, tol::Float64=PROB_TOL) =
    findall(bh.B[1:bh.K, z] .> tol)

valid_var_k_inds(z::Node, bh::VBHierarchy, tol::Float64=PROB_TOL) =
    findall(bh.Ω[1:bh.K, z] .> tol)


logbeta_vec(x) = sum(sp_loggamma.(x)) - sp_loggamma(sum(x))


nz_nodes_v1(bh::VBHierarchy) = nzcols(bh.adjmat[:, 1:bh.max_z])

function compute_V(bh::VBHierarchy)
    node_q = Queue{Tuple{Node, Int, Float64}}()

    z, ℓ, acc_s = 1, 1, 0.
    enqueue!(node_q, (z, ℓ, acc_s))

    output = Dict{Node, Float64}()
    while !isempty(node_q)
        zp, ℓ, acc_s = dequeue!(node_q)
        zs = get_zps(bh, zp)

        if isempty(zs)
            # it means zp is a leaf node
            output[zp] = acc_s
        else
            dgm = sum(digamma.(bh.A[zs, 2])
                      .- digamma.(sum(bh.A[zs, 1:2], dims=2)))
            ℓ += 1
            for z in zs
                dgm -= digamma(bh.A[z, 2])
                any(!iszero, bh.z_x[z, :]) &&
                    enqueue!(node_q, (z, ℓ, digamma(bh.A[z, 1])+dgm+acc_s))
            end
        end
    end

    log_p_v = 0.
    for (z, val) in (output)
        log_p_v += val * nnz(bh.z_x[z, :])
    end

    return log_p_v
end
