# It is valid in a VBH with tree properly rotated, i.e. the
# parent node has a smaller index than the descendents
parent(z::Node, bh::VBHierarchy) =
    z == 1 ? 0 : findfirst(!iszero, bh.adjmat[z, 1:z-1])

get_zps(bh::VBHierarchy, z::Node) = findall(!iszero, bh.adjmat[:, z])

function get_path_from_leaf!(v::NodeVec, z::Node, bh)
    v[end] = z
    @inbounds foreach(ℓ -> v[ℓ] = parent(v[ℓ+1], bh), bh.L:-1:1)
end

@inline get_path_from_leaf(z::Node, bh)::NodeVec = let
    v = NodeVec(undef, bh.L+1)
    get_path_from_leaf!(v, z, bh)
    return v
end

# @inline get_path_from_leaf(z::Node, bh::VBHierarchy)::NodeVec = let
#     zs = NodeVec()
#     z_s = z
#     while z_s != 0
#         push!(zs, z_s)
#         z_s = parent(z, bh)
#     end
#     reverse(zs)
# end

@inline function assign_paths!(bh::VBHierarchy)
    clean!(bh.z_x)
    leaf_inds = get_leaf_inds(bh)
    # The following returns cartesian indices
    @inbounds data_n_leaf = argmax(bh.Λ[:, leaf_inds], dims=2)

    v = NodeVec(undef, bh.L+1)
    @inbounds for cart_ind in data_n_leaf
        z = leaf_inds[cart_ind[2]]
        get_path_from_leaf!(v, z, bh)
        bh.z_x[v, cart_ind[1]] .= 1
    end
end

# @inline function assign_paths!(bh::VBHierarchy)
#     clean!(bh.z_x)
#     leaf_inds = get_leaf_inds(bh)
#
#     z_ind::Int = 0
#     z::Int = 0
#     v = NodeVec(undef, bh.L+1)
#     @inbounds for ind in 1:bh.n
#         z_ind = rand_log_discrete(Vector(bh.Λ[ind, leaf_inds]))
#         z = leaf_inds[z_ind]
#         get_path_from_leaf!(v, z, bh)
#         bh.z_x[v, ind] .= 1
#     end
# end

@inline function assign_paths!(bh::VBHierarchy, d_inds::IntVec)
    leaf_inds = get_leaf_inds(bh)
    # The following returns cartesian indices
    @inbounds data_n_leaf = findall(isone, bh.Λ[d_inds, leaf_inds])
    @assert size(data_n_leaf, 1) == length(d_inds)

    v = NodeVec(undef, bh.L+1)
    z::Node = 0
    @inbounds for (i, cart_ind) in enumerate(data_n_leaf)
        z = leaf_inds[cart_ind[2]]
        get_path_from_leaf!(v, z, bh)
        bh.z_x[v, d_inds[i]] .= 1
    end
end

"Especially for evaluating the purity score"
get_labels(bh::VBHierarchy, layer::Int=1)::Vector{Int} =
    [get_zps(bh.z_x, i)[layer+1] for i=1:bh.n]

function enqueue_chds!(z::Node, ℓ::Int, node_q::Deque, bh::VBHierarchy)
    zps = findall(!iszero, bh.adjmat[:, z])
    @inbounds for zp in zps; push!(node_q, (zp, ℓ+1)); end
end

function get_vs(z::Node, zb::Node, bh::VBHierarchy)::Tuple{NodeVec, NodeVec}
    vs = findall(!iszero, bh.leaves[zb+1:z])
    z_s = parent(z, bh)
    zps = get_zps(bh, z_s)
    vs_til = findall(!iszero, bh.leaves[z+1:zps[end]])
    vs, vs_til
end

function Λ_for_non_leaves(bh::VBHierarchy)::Tuple{SpMCSC, IArray2D}
    leaf_inds = get_leaf_inds(bh)
    leave_set = BitSet()
    leaf_ind_mat::IArray2D = fill([], bh.L+1)

    resize!(leaf_ind_mat[end], length(leaf_inds))
    copyto!(leaf_ind_mat[end], leaf_inds)

    Λ_z = copy(bh.Λ)
    for ℓ = bh.L+1:-1:2
        # The following line could be used for sanity check
        # @assert !isempty(leaf_ind_mat[ℓ])
        for z in leaf_ind_mat[ℓ]
            s_z = parent(z, bh)
            push!(leave_set, s_z)
            Λ_z[:, s_z] .+= Λ_z[:, z]
        end
        leaf_ind_mat[ℓ-1] = collect(leave_set)
        empty!(leave_set)
    end
    Λ_z, leaf_ind_mat
end

num_nodes(bh::VBHierarchy) = length(bh.valid_zs)


@inbounds function adjust_node_matrix!(bh::VBHierarchy)
    Is::Vector{Int} = []
    Js::Vector{Int} = []
    index::Int = 0
    nodemap = Dict{Int, Int}(1 => 1)

    bh.n_z_x = sum(bh.z_x, dims=2) |> sparsevec
    zinds = findall(iszero, bh.n_z_x)
    bh.adjmat[zinds, :] .= 0
    bh.adjmat[:, zinds] .= 0

    dropzeros!(bh.n_z_x)
    dropzeros!(bh.adjmat)

    I, J, _ = findnz(bh.adjmat)
    dim = size(bh.adjmat, 1)
    Is, Js = zeros(dim), zeros(dim)
    Iset, Jset = BitSet(I), BitSet(J)
    s_offset = e_offset = 0
    for i = 1:dim
        if i ∉ Iset && i ∉ Jset
            index = dim - e_offset
            Is[index] = i
            Js[index] = i
            nodemap[i] = index
            e_offset += 1
        else
            index = 1 + s_offset
            Is[index] = i
            Js[index] = i
            nodemap[i] = index
            s_offset += 1
        end
    end
    bh.adjmat = permute(bh.adjmat, Is, Js)
    dropzeros!(bh.adjmat)

    Is, Js = zeros(dim), Vector(1:size(bh.z_x, 2))
    s_offset = e_offset = 0
    for i = 1:dim
        if !haskey(nodemap, i)
            Is[end-e_offset] = i
            e_offset += 1
        else
            Is[nodemap[i]] = i
        end
    end
    bh.z_x = permute(bh.z_x, Is, Js)

    bh.n_z_x = sum(bh.z_x, dims=2) |> sparsevec

    return nodemap
end
