"""
Define the type Hierarchy
Provide property functions for the hierarchies
"""
mutable struct Hierarchy
    X::Union{SpMCSC, Matrix}
    Z::IArray2D
    m::Int
    n::Int
    parents::NodeVec
    siblings::IArray2D
    tree::DiGraph

    Hierarchy() = new()

    function Hierarchy(X::Union{SpMCSC, Matrix}, Z::IArray2D)
        this = new()
        this.X = X
        this.Z = Z
        this.m = size(Z, 1)
        this.n = size(X, 1)
        this.parents = []
        this.siblings = []
        this.tree = DiGraph(this.m+this.n)
        this
    end
end

function consctruct_hier!(h::Hierarchy, X::Union{SpMCSC, Matrix}, Z::IArray2D)
    h.X = X
    h.n = size(X, 1)
    h.Z = Z
    h.m = size(Z, 1)
    h.tree = DiGraph(h.m+h.n)
end

"""
Given the hierarhcy, returns an array of parents for each node (index
"""
function parents(h::Hierarchy)::NodeVec
    Z::IArray2D = h.Z
    parents::NodeVec = zeros(h.m+h.n)

    for i in 1:size(Z, 1)
        parents[Z[i]] = i + h.n
    end
    parents[end] = h.n + h.m + 1
    parents
end

function siblings(h::Hierarchy)::IArray2D
    z::IntSet
    parent_id::Int
    sibs::IArray2D

    Z::IArray2D = h.Z
    for i in 1:h.m+h.n
        parent_id = parents[i]
        z = Z[parent_id-h.n] |> IntSet
        delete!(z, i)
        push!(sibs, collect(z))
    end
    sibs
end

function node_size(h::Hierarchy)::NodeVec
    nodes::SparseVector{Int}

    Z::IArray2D = h.Z
    node_sizes = zeros(h.m+h.n)

    node_sizes[1:h.n] = 1
    for (i, nodes) in enumerate(Z)
        node_sizes[n+i] = sum(node_sizes[nodes])
    end
    node_sizes
end

function items_at_nodes!(h::Hierarchy)
    mat = spzeros(h.m+h.n, h.n)
    mat[1:h.n, 1:h.n] = eye(h.n)

    for i = 1:h.m
        indices = items_at_node(h, i+h.n)
        mat[i+h.n, indices] = 1
    end
    h.items_nodes = mat
end

items_at_nodes(h::Hierarchy)::IArray2D = [item_at_node(h, i+h.n) for i = 1:h.m]

"""
Need to optimise the code later
"""
function items_at_node(h::Hierarchy, idx::Int)::Vector{Int}
    items::Vector{Int}

    Z::IArray2D = h.Z

    idx > h.n + h.m &&
        throw("The index is out of bound! Index=$indx, m=$m, n=$n")

    idx <= h.n ? [idx] : map(items_at_node, product(h, Z[i-h.n]))
end

function construct_tree!(t::DiGraph, Z::IArray2D, n::Int)
    for i in size(Z, 1):-1:1, node in Z[i]
        @assert !has_edge(t, i+n, node)
            "getting repeated edges means the other programs have problems"
        @assert add_edge!(t, i+n, node)
            "m and n has problems $t $n $i $(i+n) $node"
    end
    @assert t |> SimpleGraph |> is_connected
end

function Z_to_tree(n::Int, Z::IArray2D)::AbstractGraph
    t = DiGraph(size(Z, 1)+n)
    construct_tree!(t, Z, n)
    t
end

function Z_to_tree!(h::Hierarchy)
    tree = h.tree
    construct_tree!(h.tree, h.Z, h.n)
end

function tree_to_Z(m::Int, n::Int, tree::AbstractGraph)::IArray2D
    Z::IArray2D = fill([], m)
    for edge in edges(tree)
        sidx, eidx = src(edge), dst(edge)
        ridx = sidx - n                         # real node index in Z
        ridx > 0 && push!(Z[ridx], eidx)        # assign 1 to the corresponding entry in the adjacency matrix
    end
    Z
end

function save_hier(h::Hierarchy, dataname::String; path::String="")
    datastr = JSON.json(h.Z)
    f4z = string(dataname, "_Z.json")
    savejson(datastr, f4z; path=path)

    f4tree = string(dataname, "_tree")
    savegraph(h.tree, f4tree; path=path)
end

function Z_to_etetree(n::Int, Z::IArray2D)::String
    m = size(Z, 1)

    strs::Vector{String} = map(string, 1:m+n)
    for i in 1:m
        tree_str = join(strs[Z[i]], ",")
        strs[i+n] = "($tree_str)$i"
    end

    ete_tree = "($(last(strs)));"
    ete_tree
end

function Z_to_etetree(n::Int, Z::IArray2D, names::Vector{String})::String
    @assert n == length(names)
    m = size(Z, 1)

    strs::Vector{String} = map(string, 1:m+n)
    for i = 1:n
        strs[i] = names[i]
    end

    for i in 1:m
        tree_str = join(strs[Z[i]], ",")
        strs[i+n] = "($tree_str)$i"
    end

    ete_tree = "($(last(strs)));"
    ete_tree
end

function Z_to_etetree_v2(n::Int, Z::IArray2D, names::Vector{String})::String
    @assert n == length(names)
    m = size(Z, 1)

    strs::Vector{String} = map(string, 1:m+n)
    strs[1:n] .= names[:]

    for i in 1:m
        # If all children are single leafs,
        # we group them as a new leaf
        # which is more convenient for plotting the hierarchy.
        # If there is one non-leaf node, we should keep them
        # as separate children
        tree_str = all(Z[i] .≤ n) ?
            join(strs[Z[i]], "_") : join(strs[Z[i]], ",")

        strs[i+n] = "($tree_str)$i"
        println(strs[i+n], "\n")
    end

    return "($(last(strs)));"
end


##### Plotting utility
function get_hierarchical_labels!(labels::Matrix{Int}, h::Hierarchy)
    children_list = h.children_list
    levels = size(labels, 2)
    nodes = h.Z[end]
    tmp_nodes::Vector{Int} = []
    i = 1
    while i <= levels
        counter = 0
        for (j, node) in enumerate(nodes)
            labels[h.items_at_nodes[node], i] = j
            if node > n
                for n in children_list[node]
                    push!(tmp_nodes, n)
                end
            else
                push!(tmp_nodes, node)
            end
        end
        nodes = copy(tmp_nodes)
        i += 1
    end
end
