@inline num_data(bh::BHierarchy) = bh.n

@inline function clean_redundant_nodes!(bh::BHierarchy; check=false)
    zs = findall(!iszero, bh.n_z_x)
    @inbounds for z in zs[2:end]
        parent = get_parent(z, bh)

        if bh.n_z_x[z] == bh.n_z_x[parent]
            zps = get_zps(z, bh)

            bh.adjmat[z, parent] = 0
            bh.adjmat[zps, z] .= 0
            bh.adjmat[zps, parent] .= 1

            bh.n_z_x[z] = 0
            bh.n_z_k[z, :] .= 0
            bh.z_x[z, :] .= 0
        end
    end
    clean_sparse!(bh)
end

@inbounds function adjust_node_matrix!(bh::BHierarchy)
    Is::Vector{Int} = []
    Js::Vector{Int} = []
    index::Int = 0
    nodemap = Dict{Int, Int}(1 => 1)

    zinds = findall(iszero, bh.n_z_x)
    bh.adjmat[zinds, :] .= 0
    bh.adjmat[:, zinds] .= 0

    bh.B[:, zinds] .= 0
    dropzeros!(bh.n_z_x)
    dropzeros!(bh.adjmat)
    dropzeros!(bh.B)

    I, J, _ = findnz(bh.adjmat)
    dim = size(bh.adjmat, 1)
    Is, Js = zeros(dim), zeros(dim)
    Iset, Jset = BitSet(I), BitSet(J)
    s_offset = e_offset = 0
    for i = 1:dim
        if i ∉ Iset && i ∉ Jset
            index = dim - e_offset
            Is[index] = i
            Js[index] = i
            nodemap[i] = index
            e_offset += 1
        else
            index = 1 + s_offset
            Is[index] = i
            Js[index] = i
            nodemap[i] = index
            s_offset += 1
        end
    end
    bh.adjmat = permute(bh.adjmat, Is, Js)
    dropzeros!(bh.adjmat)

    Is, Js = zeros(dim), Vector(1:size(bh.z_x, 2))
    s_offset = e_offset = 0
    for i = 1:dim
        if !haskey(nodemap, i)
            Is[end-e_offset] = i
            e_offset += 1
        else
            Is[nodemap[i]] = i
        end
    end
    bh.z_x = permute(bh.z_x, Is, Js)

    Js, Is = Is, Vector(1:size(bh.B, 1))
    bh.B = permute(bh.B, Is, Js)

    bh.n_z_x = sum(bh.z_x, dims=2) |> sparsevec

    bh.n_z_k = bh.z_x * bh.x_k

    bh.max_node = nnz(bh.n_z_x)

    return nodemap
end

@inline function convert2hierarchy(bh::BHierarchy,
                                   cl_redun=false; verbose=false)::Hierarchy
    h = Hierarchy()
    convert2hierarchy!(h, bh, cl_redun)
    h
end

@inline function convert2hierarchy!(h::Hierarchy, bh::BHierarchy,
                                    cl_redun::Bool=false)
n = bh.n
    m = num_nodes(bh)

    parent4x = 0
    offset = n + m + 1
    Z::IArray2D = fill([], offset-1)

    conv_dict = Dict{Int, Int}()
    current_node = n+1
    the_node = 0

    @inbounds for i = 1:n
       parent4x = findlast(!iszero, bh.z_x[:, i])
       if !haskey(conv_dict, parent4x)
           push!(conv_dict, parent4x => current_node)
           current_node += 1
       end
       the_node = conv_dict[parent4x]

       push!(Z[the_node], i)
    end

    leaves = keys(conv_dict)
    node_q = Queue{Tuple{Int, Int}}()
    for (k, v) in conv_dict
       enqueue!(node_q, (k, v))
    end

    while !isempty(node_q)
       z, conv_z = dequeue!(node_q)

       @assert nnz(bh.adjmat[z, :]) == 1 nnz(bh.adjmat[z, :])
       parent4z = findfirst(!iszero, bh.adjmat[z, :]) # parent(bh, z)
       if !haskey(conv_dict, parent4z)
           push!(conv_dict, parent4z => current_node)
           current_node += 1
           parent4z == 1 || enqueue!(node_q, (parent4z, conv_dict[parent4z]))
       end
       the_node = conv_dict[parent4z]

       push!(Z[the_node], conv_z)
    end

    @assert conv_dict[1] == n + m

    consctruct_hier!(h, copy(bh.X'), Z[1+n:end])
    Z_to_tree!(h)
end

num_nodes(bh::BHierarchy) = nnz(bh.n_z_x)

"Especially for evaluating the purity score"
get_labels(bh::BHierarchy,
           layer::Int=1)::Vector{Int} =
    [get_zps(bh.z_x, i)[layer+1] for i=1:bh.n]

"
This function will be called when the initialized
sparse matrices used for the bayesian hierarchy are full.
"
function expand_bh_mat!(bh::BHierarchy, if_leaves::Bool=false)
    n = num_data(bh)

    dim = length(bh.n_z_x)
    I, V = findnz(bh.n_z_x)
    n_z_x = sparsevec(I, V, dim+n)
    bh.n_z_x = n_z_x

    rdim, cdim = size(bh.n_z_k)
    I, J, V = findnz(bh.n_z_k)
    n_z_k = sparse(I, J, V, rdim+n, cdim)
    bh.n_z_k = n_z_k

    rdim, cdim = size(bh.adjmat)
    I, J, V = findnz(bh.adjmat)
    adjmat = sparse(I, J, V, rdim+n, cdim+n)
    bh.adjmat = adjmat

    rdim, cdim = size(bh.z_x)
    I, J, V = findnz(bh.z_x)
    z_x = sparse(I, J, V, rdim+n, cdim)
    bh.z_x = z_x

    rdim, cdim = size(bh.B)
    I, J, V = findnz(bh.B)
    B = sparse(I, J, V, rdim, cdim+n)
    bh.B = B

    if if_leaves
        dim = length(bh.leaves)
        I, V = findnz(bh.leaves)
        leaves = sparsevec(I, V, dim+n)
        bh.leaves = leaves
    end
end

function save_bhier(bh::BHierarchy, dataname::String; path::String="")
    f4z = joinpath(path, string(dataname, BHIER_POSTFIX))
    savejld(bh, f4z; vname="bh")
end

function load_bhier(dataname::String; path::String="")::BHierarchy
    f4z = joinpath(path, string(dataname, BHIER_POSTFIX))
    loadjld(f4z, "bh")
end

get_parent(z::Node, bh::BHierarchy) = findfirst(!iszero, bh.adjmat[z, :])

"get a list of z prime"
get_zps(z::Node, bh::BHierarchy) = findall(!iszero, bh.adjmat[:, z])

leaf_4_ind(ind::Int, bh::BHierarchy) = findlast(!iszero, bh.z_x[:, ind])

comp_4_ind(ind::Int, bh::BHierarchy) = findfirst(!iszero, bh.x_k[ind, :])
