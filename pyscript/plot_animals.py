from pathlib import Path
import ete3

def plot(metr, C, ep, run_index):
    with open(("animals_output/{}_{}/animals_test_tree_string_{}_{}.txt"
               .format(C, ep, metr, run_index)), "r") as f:
        tstr = f.read()

    tstr = tstr.strip()

    t = ete3.Tree(tstr)
    ts = ete3.TreeStyle()
    ts.show_leaf_name = True
    ts.mode = "r"
    # ts.arc_start = -180 # 0 degrees = 3 o'clock
    # ts.arc_span = 180
    ts.rotation = 90

    ns = ete3.NodeStyle()
    ns["size"] = 0
    ns["hz_line_width"] = 1
    ns["vt_line_width"] = 1

    for n in t.traverse():
        n.set_style(ns)

#     t.show(tree_style=ts)
    (Path("animals_output/{}_{}".format(C, ep))
        .mkdir(parents=True, exist_ok=True))
    t.render('animals_output/{}_{}/animals_candidate_{}_{}_{}_{}.pdf'
             .format(C, ep, metr, C, ep, run_index),
             tree_style=ts)

if __name__ == '__main__':
    import sys 
    run_index = 1 if len(sys.argv) <= 1 else sys.argv[1]    
    C = 'random' if len(sys.argv) <= 2 else sys.argv[2]    
    ep = 'random' if len(sys.argv) <= 3 else sys.argv[3]    
    
    plot('mcl', C, ep, run_index)
