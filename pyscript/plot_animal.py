import ete3

def plot(metr):
    with open(("animals_output/animals_test_tree_string_{}.txt"
               .format(metr)), "r") as f:
        tstr = f.read()

    tstr = tstr.strip()

    t = ete3.Tree(tstr)
    ts = ete3.TreeStyle()
    ts.show_leaf_name = True
    ts.mode = "r"
    # ts.arc_start = -180 # 0 degrees = 3 o'clock
    # ts.arc_span = 180
    ts.rotation = 90

    ns = ete3.NodeStyle()
    ns["size"] = 0
    ns["hz_line_width"] = 1
    ns["vt_line_width"] = 1

    for n in t.traverse():
        n.set_style(ns)

#     t.show(tree_style=ts)
    t.render('animals_output/animals_candidate_{}.pdf'.format(metr),
             tree_style=ts, dpi=1200)


plot('mcl')
