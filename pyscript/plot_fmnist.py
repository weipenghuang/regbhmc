import os
import ete3
from pathlib import Path


img_face_dict = {}
path = './data/fmnist/pngs/'

for f in os.listdir(path):
    ind = f.split('_')[-1].split('.')[0]
    img_face_dict[ind] = ete3.faces.ImgFace(os.path.join(path, f))
    img_face_dict[ind].rotable = False

def mylayout(node):
    n_cols = 5
    if ((len(node.get_children()) == 1 and not node.is_leaf())
        or node.is_root()):
        return

    if len(node) != 100:
        for i, ind in enumerate(set(node.get_leaf_names())):
            col = i % n_cols
            # Add the corresponding face to the node
            ete3.faces.add_face_to_node(img_face_dict[ind],
                                        node, column=col)
    elif node.is_leaf():
        node.img_style['size'] = 0



def plot(metr, C, ep, run_index):
    with open(("fmnist_output/{}_{}/fmnist_tree_string_{}_{}.txt"
               .format(C, ep, metr, run_index)), "r") as f:
        tstr = f.read()

    tstr = tstr.strip()

    t = ete3.Tree(tstr)
    ts = ete3.TreeStyle()
    ts.show_leaf_name = False
    ts.mode = "r"
    ts.rotation = 90

    # ts.arc_start = -180 # 0 degrees = 3 o'clock
    # ts.arc_span = 180

    ns = ete3.NodeStyle()
    ns["size"] = 0
    ns["hz_line_width"] = 1.5
    ns["vt_line_width"] = 1.5

    for n in t.traverse():
        n.set_style(ns)

    ts.layout_fn = mylayout
#     t.show(tree_style=ts)
    (Path("fmnist_output/{}_{}".format(C, ep))
        .mkdir(parents=True, exist_ok=True))
    t.render("fmnist_output/{}_{}/fmnist_{}_{}_{}_{}.pdf"
             .format(C, ep, metr, C, ep, run_index),
             tree_style=ts, dpi=1200)


for metr in ['mcl']:
    import sys 
    run_index = 1 if len(sys.argv) <= 1 else sys.argv[1]    
    C = 'random' if len(sys.argv) <= 2 else sys.argv[2]    
    ep = 'random' if len(sys.argv) <= 3 else sys.argv[3]    
    
    plot('mcl', C, ep, run_index)
 
