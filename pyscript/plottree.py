import os
import ete3

img_face_dict = {}
path = './data/fmnist/pngs/'

for f in os.listdir(path):
    ind = f.split('_')[-1].split('.')[0]
    img_face_dict[ind] = ete3.faces.ImgFace(os.path.join(path, f))
    img_face_dict[ind].rotable = False


def mylayout(node):
    n_cols = 5
    if ((len(node.get_children()) == 1 and not node.is_leaf()) 
        or node.is_root()): 
        return 
    
    if len(node) != 100:
        for i, ind in enumerate(set(node.get_leaf_names())):
            col = i % n_cols
            # Add the corresponding face to the node
            ete3.faces.add_face_to_node(img_face_dict[ind], 
                                        node, column=col)
    elif node.is_leaf():
        node.img_style['size'] = 0


def main(idx):
    for mth in ['rsvi', 'rvi']:
        with open("fmnist_output/{}/fmnist_test_tree_string_{}.txt"
                  .format(mth, idx), "r") as f:
            tstr = f.read()

            tstr = tstr.strip()

            tstr = tstr.replace(' [', '-').replace(']', '')

            t = ete3.Tree(tstr)
            ts = ete3.TreeStyle()
            ts.show_leaf_name = False
            ts.mode = "r"
            ts.rotation = 90

            # ts.arc_start = -180 # 0 degrees = 3 o'clock
            # ts.arc_span = 180

            ns = ete3.NodeStyle()
            ns["size"] = 0
            ns["hz_line_width"] = 4
            ns["vt_line_width"] = 4

            for n in t.traverse():
                n.set_style(ns)

            ts.layout_fn = mylayout
            #t.show(tree_style=ts)
            t.render("fmnist_output/tree_plots/mnist-fashion-{}-{}.pdf"
                     .format(mth, idx), 
                     tree_style=ts, dpi=1200)
            # t.render('fmnist_candidate.pdf', tree_style=ts, dpi=1200)


if __name__ == '__main__':
    import sys
    idx = 1 if len(sys.argv) <= 1 else sys.argv[1]
    main(idx)

    #for i in range(1, 17):  
    #    main(i)
