import itertools

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import seaborn as sns
from sklearn import mixture

sns.set_context('poster')
sns.set(style='white', font_scale=3)

dfs = []

methods = ['slice']
data_names = ['compound']
markers = ['o', '+', '*', 'v', 'h', 'x', 'd', 's', 'p', '>']
#colors = ['navy', 'c', 'cornflowerblue', 'gold', 
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 
          'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan', 
          'black']
for _, method in enumerate(methods):
    plt.figure(figsize=(11.2, 8.1))

    for _, data_name in enumerate(data_names):
        
        mat = np.load('./output/{}_{}.npy'
                       .format(data_name, method))

        df = pd.read_csv('./data/{}/{}.csv'.format(data_name, data_name),
                         sep=' +|\t', engine='python', header=None)
        data = df.values

        #fig, axes = plt.subplots(1, mat.shape[0], figsize=(33, 8.4))
        
        for i in range(1):
            try:
                classes = np.unique(mat[i, :])
                print(classes)
                for j, k in enumerate(classes):
                    inds = np.where(mat[i, :]==k)[0]
               
                    plt.scatter(data[inds, 0], data[inds, 1], 
                                    marker=markers[j], s=50, 
                                    c=colors[j])
            except:
                pass
    plt.xticks(())
    plt.yticks(())


    #plt.tight_layout()
    plt.savefig('{}_2D.pdf'.format(method))
    plt.close('all')




color_iter = itertools.cycle(['navy', 'c', 'cornflowerblue', 'gold',
                              'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
                              'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan',
                              'black'])
sns.set_context('poster')

def plot_results(X, Y_, means, covariances, index, title):
    plt.figure(figsize=(11.2, 8.1))
    splot = plt.subplot(1, 1, 1 + index,)
    for i, (mean, covar, color) in enumerate(zip(
            means, covariances, color_iter)):

        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(Y_ == i):
            continue
        plt.scatter(X[Y_ == i, 0], X[Y_ == i, 1], 1, color=color)

        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(splot.bbox)
        ell.set_alpha(0.5)
        splot.add_artist(ell)

#     plt.xlim(-9., 5.)
#     plt.ylim(-3., 6.)
    plt.xticks(())
    plt.yticks(())

P = np.load('./output/P.npy')
means = np.load('./output/means.npy').T
covs = np.load('./output/covs.npy')

data = pd.read_csv('./data/compound/compound.csv', header=None, sep='\t')

X = data.values[:, :2] 
revised_P = np.array([np.argmax(P[i, :]) for i in range(X.shape[0])], 
                     dtype=int)
plot_results(X, revised_P, means, covs, 0, 'BHMC')

plt.savefig('mixture.pdf')    
