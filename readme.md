The code is implemented in Julia (any version later than 1.6 should work). 
To install the required packages, we can simply open an console and use the following code:

```julia
using Pkg
Pkg.add("CSV")
Pkg.add("DataFrames")
using CSV, DataFrames

df = CSV.File("REQUIRE") |> DataFrame
for p in df[:, 1]
    Pkg.add(p)
end
```

Exit the Julia console. Now, we can modify the files in the *bash_files* folder for runing the programs. 

**Please note that our implementation of the Generalized Inverse Gaussian is from https://github.com/LMescheder/GenInvGaussian.jl.**