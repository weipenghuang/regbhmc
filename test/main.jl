using Clustering: hclust, Hclust, randindex, varinfo
using DataFrames
using Random
using FreqTables
using ArgParse

path = normpath(joinpath(dirname(@__FILE__), "../src"))
push!(LOAD_PATH, path)
using BHMC


function get_labels_from_merge(merge::Matrix{Int64}, N::Int)
    m, n = size(merge)
    labels::Vector{Int} = zeros(N)
    for i in 1:m, j in 1:n
        if merge[i, j] < 0
            idx = -merge[i, j]
            labels[idx] = i
        end
    end
    labels
end

function main()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--datanames", "-d"
            help = "input the data names, split by comma"
            arg_type = String
            required = false
        "--methods", "-m"
            help = "input the methods, split by comma"
            arg_type = String
            required = false
        "--num_loops", "-l"
            help = "number of loops for repeating the simulations"
            arg_type = Int
            required = false
        "--max_loops"
            help = "number of loops for the MCMC"
            arg_type = Int
            required = false
        "--if_save_tree"
            help = "if we save the tree"
            arg_type = Bool
            required = false
        "--if_plot_tree"
            help = "if we plot the tree"
            arg_type = Bool
            required = false
        "--if_compute_score"
            help = "if we compute the score and print them"
            arg_type = Bool
            required = false
        "--burnin"
            help = "Burn in number for MH sampler"
            arg_type = Int
            required = false
        "--alpha"
            help = "Hyperparameter alpha"
            arg_type = Float64
            required = false
        "--gamma"
            help = "Hyperparameter gamma"
            arg_type = Float64
            required = false
        "--eta"
            help = "Hyperparameter eta"
            arg_type = Float64
            required = false
        "--lambda"
            help = "Hyperparameter lambda for NIW"
            arg_type = Float64
            required = false
        "--nu"
            help = "Hyperparameter nu for NIW"
            arg_type = Float64
            required = false
        "--epsilon0"
            help = "Hyperparameter epsilon0"
            arg_type = Float64
            required = false
        "--nu0"
            help = "Hyperparameter nu0"
            arg_type = Float64
            required = false
        "--C"
            help = "Hyperparameter C"
            arg_type = Float64
            required = false
        "--disc"
            help = "Hyperparameter discount"
            arg_type = Float64
            required = false
        "--J"
            help = "varrho for the reg term"
            arg_type = Float64
            required = false
        "--L"
            help = "The maximum length for the hierarchy"
            arg_type = Int
            required = false
        "--conv"
            help = "If tracking the convergence of the posterior"
            arg_type = Bool
            required = false
        "--seed"
            help = "The random seed"
            arg_type = Int
            required = false
    end
    parsed_args = parse_args(s)

    # general settings for all methods
    datanames::Vector{String} = parsed_args["datanames"] == nothing ?
        ["compound", "r15", "ideal"] :
        split(parsed_args["datanames"], ",")

    methods::Vector{String} = parsed_args["methods"] == nothing ?
        ["gibbs"] :
        split(parsed_args["methods"], ",")

    num_loops::Int = parsed_args["num_loops"] == nothing ?
        1 : parsed_args["num_loops"]

    max_loops::Int = parsed_args["max_loops"] == nothing ?
        100 : parsed_args["max_loops"]

    if_save_tree::Bool = parsed_args["if_save_tree"] == nothing ?
        false : parsed_args["if_save_tree"]

    if_plot_tree::Bool = parsed_args["if_plot_tree"] == nothing ?
        false : parsed_args["if_plot_tree"]

    if_compute_score::Bool = parsed_args["if_compute_score"] == nothing ?
        false : parsed_args["if_compute_score"]

    alpha = parsed_args["alpha"] === nothing ? .2 : parsed_args["alpha"]

    gamma = parsed_args["gamma"] === nothing ? 1.5 : parsed_args["gamma"]

    eta = parsed_args["eta"] === nothing ? .1 : parsed_args["eta"]

    lambda = parsed_args["lambda"] === nothing ? .01 : parsed_args["lambda"]

    nu = parsed_args["nu"] === nothing ? 90 : parsed_args["nu"]

    J = parsed_args["J"] === nothing ? 0.001 : parsed_args["J"]

    epsilon0 = parsed_args["epsilon0"] === nothing ? 1 : parsed_args["epsilon0"]

    nu0 = parsed_args["nu0"] === nothing ? .01 : parsed_args["nu0"]

    C = parsed_args["C"] === nothing ? .1 : parsed_args["C"]

    disc = parsed_args["disc"] === nothing ? .75 : parsed_args["disc"]

    L = parsed_args["L"] === nothing ? 3 : parsed_args["L"]

    conv = parsed_args["conv"] === nothing ? false : parsed_args["conv"]

    burnin = parsed_args["burnin"] == nothing ? 5 : parsed_args["burnin"]

    seed = parsed_args["seed"] == nothing ? 2020 : parsed_args["seed"]
    Random.seed!(seed)

    for method in methods, dataname in datanames
        if method == "gibbs"
            for i = 1:num_loops
                @time sim_gibbs(dataname;
                                run_index=i,
                                burnin=burnin,
                                if_save_tree=if_save_tree,
                                if_plot_tree=if_plot_tree,
                                if_compute_score=if_compute_score,
                                L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                conv=conv)
            end
        elseif method == "slice"
            sd = 1
            i = 1
            Threads.@threads for i in 1:num_loops
            # for i = 1:num_loops
                @time sim_slice(dataname;
                                run_index=i,
                                burnin=burnin,
                                if_save_tree=if_save_tree,
                                if_plot_tree=if_plot_tree,
                                if_compute_score=if_compute_score,
                                L=L, α=alpha, γ0=gamma,
                                η=eta, λ=lambda, ν=nu,
                                ν0=nu0, ϵ0=epsilon0, C=C, disc=disc,
                                max_loops=max_loops,
                                conv=conv, seed=sd)
            end
        elseif method == "mh"
            for i = 1:num_loops
                @time sim_mh(dataname;
                             run_index=i,
                             burnin=burnin,
                             if_save_tree=if_save_tree,
                             if_plot_tree=if_plot_tree,
                             if_compute_score=if_compute_score,
                             L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                             conv=conv)
            end
        elseif method == "vi"
            burnin = parsed_args["burnin"] == nothing ? 5 : parsed_args["burnin"]
            for i = 1:num_loops
                @time sim_vi(dataname;
                             run_index=i,
                             max_loops=max_loops,
                             if_save_tree=if_save_tree,
                             if_plot_tree=if_plot_tree,
                             if_compute_score=if_compute_score,
                             L=L, α=alpha, γ0=gamma, η=eta, J=0,
                             conv=conv)
             end
         elseif method == "svi"
             for i = 1:num_loops
                 @time sim_svi(dataname;
                               run_index=i,
                               max_loops=max_loops,
                               if_save_tree=if_save_tree,
                               if_plot_tree=if_plot_tree,
                               if_compute_score=if_compute_score,
                               L=L, α=alpha, γ0=gamma, η=eta, J=0,
                               conv=conv)
            end
        elseif method == "rsvi"
            for i = 1:num_loops
                sim_rsvi(dataname;
                         run_index=i,
                         max_loops=max_loops,
                         if_save_tree=if_save_tree,
                         if_plot_tree=if_plot_tree,
                         if_compute_score=if_compute_score,
                         L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                         conv=conv, J=J)
           end
        elseif method == "rvi"
           for i = 1:num_loops
               sim_rvi(dataname;
                       run_index=i,
                       max_loops=max_loops,
                       if_save_tree=if_save_tree,
                       if_plot_tree=if_plot_tree,
                       if_compute_score=if_compute_score,
                       L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                       conv=conv, J=J)
            end
        end
    end
end

main()
