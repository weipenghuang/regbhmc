using DataFrames
using Random
using ArgParse
# using Gadfly
# using ColorBrewer
# import Cairo
# Gadfly.push_theme(Theme(major_label_font_size=20pt,
#                         minor_label_font_size=20pt))

# Random.seed!(777)
path = normpath(joinpath(dirname(@__FILE__), "../src"))
push!(LOAD_PATH, path)
using BHMC


function plot_samples(n::Int, d::Int; kwargs...)
    bh = sample_from_tree(n, kwargs...)

    df = DataFrame(x=Float64[], y=Float64[], color=[])
    colors = palette("Set1", 8);

    layers = []
    for (i, k) in findall(!iszero, bh.n_k_x) |> enumerate
        xinds = findall(!iszero, bh.x_k[:, k])
        push!(layers,
              layer(x=bh.X[1, xinds], y=bh.X[2, xinds],
                    Geom.point, Geom.ellipse(levels=[.99]),
                    #Geom.point, Geom.ellipse,
                    Theme(default_color=colors[i], point_size=5pt)))
    end

    # p = plot(df, x=:x, y=:y, Theme(default_color="purple", point_size=1.5pt))
    # p |> PDF("test.pdf")

    # p = plot(df, x=:x, y=:y, color=:color,
    #          Theme(point_size=1.5pt))
    set_default_plot_size(19cm, 16cm)

    p = plot(layers...)
    p |> PDF("test2.pdf")
end


function draw_samples(n::Int, d::Int; kwargs...)
    bh = sample_from_tree(n, d, kwargs...)

    savedf(DataFrame(bh.X'), "syn_$n.csv";
           path="./data/synthetic")
end


function main()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--num", "-n"
            help = "number of the samples"
            arg_type = Int
            required = false
        "--dims", "-d"
            help = "number of the dimensions"
            arg_type = Int
            required = false
        "--alpha"
            help = "Hyperparameter alpha"
            arg_type = Float64
            required = false
        "--gamma"
            help = "Hyperparameter gamma"
            arg_type = Float64
            required = false
        "--eta"
            help = "Hyperparameter eta"
            arg_type = Float64
            required = false
        "--L"
            help = "The maximum length for the hierarchy"
            arg_type = Int
            required = false
    end
    parsed_args = parse_args(s)

    num = parsed_args["num"] == nothing ? 500 : parsed_args["num"]
    dims = parsed_args["dims"] == nothing ? 5 : parsed_args["dims"]

    # plot_samples(num)
    draw_samples(num, dims)
end

@time main()
