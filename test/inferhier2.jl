using Clustering: hclust, Hclust, randindex, varinfo
using DataFrames
using Random
using SparseArrays
using ArgParse
using JLD2
using CSV
using LinearAlgebra
using PDMats
using Distributions
using NPZ:npzwrite
using DataStructures:Queue

path = normpath(joinpath(dirname(@__FILE__), "../src"))
push!(LOAD_PATH, path)
using BHMC

Random.seed!(2012)


@inline function save_coocmat!(bh, label_mat, cooc_path, ind,
                               foldername, dataname)
    fill!(label_mat, 0)
    for ℓ in 1:bh.L
        label_mat[ℓ, :] .= get_labels(bh, ℓ)
    end
    cooc_path = joinpath(cooc_path, foldername)
    checkpath(cooc_path)
    CSV.write(joinpath(cooc_path, "$(dataname)_cooc_$(ind).csv"),
              DataFrame(label_mat, :auto))
end


@inline function test_animals(burnin::Int; kwargs...)
    kwargs_dict = Dict(kwargs)
    loops = kwargs_dict[:max_loops] |> Int
    run_index = kwargs_dict[:run_index]
    save_cooc = get(kwargs_dict, :savecooc, false)
    save_hier_4_hqs = get(kwargs_dict, :save_hier_4_hqs, false)

    raw_df = CSV.File("data/animals/animals_after_pca_7.csv";
                      delim=',', header=1) |> DataFrame
    X = BHMC.DoubleMat(raw_df[!, 2:end])
    names = Vector{String}(raw_df[!, 1])
    S = bhmc_regmh(X; burnin=burnin, kwargs...)

    zs = NodeVec()
    zps = NodeVec()
    best_mcle_bh = deepcopy(S.bh)
    mp = l_post = -Inf
    mcl = l_mcl = -Inf
    mp_ind = 0
    mcl_ind = 0

    dataname = "animals"
    label_mat = zeros(S.bh.L, S.bh.n)
    cooc_path = "./coocurrence/animals/"
    @inbounds for ind = 1:loops
        infer!(S, zs, zps, nothing, false)

        l_mcl = log_mcle(S.bh)
        # l_mcl = log_mle(S.bh)
        if l_mcl > mcl
            BHMC.semicopy!(best_mcle_bh, S.bh)
            mcl_ind = ind
            mcl = l_mcl
        end

        save_cooc && save_coocmat!(S.bh, label_mat, cooc_path, ind,
                                   "$(dataname)_$(S.bh.C)_$(S.bh.ϵ0)_$run_index",
                                   dataname)

        if save_hier_4_hqs
           hier = Hierarchy()
           convert2hierarchy!(hier, S.bh)
           # make it compatible with our python library
           for i = 1:size(hier.Z, 1)
               hier.Z[i] .-= 1
           end
           method = "regmh"
           save_hier(hier, "$(dataname)_$(method)_$(ind)";
                     path="./output/4hqs/$(dataname)_$(method)_$(S.bh.C)_$(S.bh.ϵ0)_$(run_index)")
        end
    end

    # max log post --> $(round(l_post; digits=2));   index --> $mp_ind
    println("
        max log mcle --> $(round(l_mcl; digits=2));    index --> $mcl_ind
    ")

    bhs = [best_mcle_bh]
    for (j, metr) in enumerate(["mcl"])
        hier = convert2hierarchy(bhs[j])
        tree_str = Z_to_etetree(hier.n, hier.Z, names)

        path = joinpath("animals_output", "$(bhs[j].C)_$(bhs[j].ϵ0)")
        checkpath(path)
        fname = joinpath(path,
                         "$(dataname)_test_tree_string_$(metr)_$(run_index).txt")
        open(fname, "w") do f
            write(f, tree_str)
        end

        hier = Hierarchy()
        convert2hierarchy!(hier, bhs[j])
        # make it compatible with our python library
        for i = 1:size(hier.Z, 1)
            hier.Z[i] .-= 1
        end
        method = bhs[j].if_reg ? "regmh" : "mh"
        save_hier(hier, "$(dataname)_$(method)_$(run_index)";
                  path="./output/$(dataname)_$(method)_$(bhs[j].C)_$(bhs[j].ϵ0)")

        # avg_i_d, avg_o_d = average_empir_full_dist(bhs[j])
        # open(joinpath(path, "$(dataname)_$(method)_$(bhs[j].C)_$(bhs[j].ϵ0)_1.0_$(run_index).dist"),
        #      "w") do io
        #     write(io, "$run_index AID: $avg_i_d; AOD: $avg_o_d")
        # end
    end
end


"""
Test on fashion MNIST data
"""
function test_fmnist(burnin::Int; kwargs...)
    raw_df = CSV.File("data/fmnist/sampled_fmnist2.csv";
                       delim=',', header=1) |> DataFrame

    X::Matrix{Float64} = BHMC.DoubleMat(raw_df[!, 2:end-1])

    inds::Vector{String} = map(string, raw_df[:, 1])
    g_labels::Vector{Int} = map(Int, raw_df[:, end])

    S = bhmc_regmh(X; burnin=burnin, scale_sig=1, kwargs...)

    zs = NodeVec()
    zps = NodeVec()
    # best_map_bh = VBMCMCHierarchy()
    best_mcle_bh = deepcopy(S.bh)
    mp = l_post = -Inf
    mcl = l_mcl = -Inf
    mp_ind = 0
    mcl_ind = 0

    kwargs_dict = Dict(kwargs)
    loops = kwargs_dict[:max_loops] |> Int
    run_index = kwargs_dict[:run_index]
    save_cooc = get(kwargs_dict, :savecooc, false)
    save_hier_4_hqs = get(kwargs_dict, :save_hier_4_hqs, false)

    dataname = "fmnist"
    label_mat = zeros(S.bh.L, S.bh.n)
    cooc_path = "./coocurrence/fmnist/"
    @inbounds for ind = 1:loops
        infer!(S, zs, zps, nothing, false)

        l_mcl = log_mcle(S.bh)
        if l_mcl > mcl
            BHMC.semicopy!(best_mcle_bh, S.bh)
            mcl_ind = ind
            mcl = l_mcl
        end

        save_cooc && save_coocmat!(S.bh, label_mat, cooc_path, ind,
                                   "$(dataname)_$(S.bh.C)_$(S.bh.ϵ0)_$run_index",
                                   dataname)

        if save_hier_4_hqs
            hier = Hierarchy()
            convert2hierarchy!(hier, S.bh)
            # make it compatible with our python library
            for i = 1:size(hier.Z, 1)
                hier.Z[i] .-= 1
            end
            method = "regmh"
            save_hier(hier, "$(dataname)_$(method)_$(ind)";
                      path="./output/4hqs/$(dataname)_$(method)_$(S.bh.C)_$(S.bh.ϵ0)_$(run_index)")
        end
    end

    println("max log mcle --> $(round(l_mcl; digits=2));    index --> $mcl_ind")

    dataname = "fmnist"
    bhs = [best_mcle_bh]
    for (j, metr) in enumerate(["mcl"])
        hier = convert2hierarchy(bhs[j])
        tree_str = Z_to_etetree(hier.n, hier.Z, inds)

        path = joinpath("$(dataname)_output", "$(bhs[j].C)_$(bhs[j].ϵ0)")
        checkpath(path)
        open(joinpath(path, "fmnist_tree_string_$(metr)_$(run_index).txt"),
             "w") do f
            write(f, tree_str)
        end

        hier = Hierarchy()
        convert2hierarchy!(hier, bhs[j])
        # make it compatible with our python library
        for i = 1:size(hier.Z, 1)
            hier.Z[i] .-= 1
        end
        method = bhs[j].if_reg ? "regmh" : "mh"
        save_hier(hier, "$(dataname)_$(method)_$(run_index)";
                  path="./output/$(dataname)_$(method)_$(bhs[j].C)_$(bhs[j].ϵ0)")

        # avg_i_d, avg_o_d = average_empir_full_dist(bhs[j])
        # open(joinpath(path, "$(dataname)_$(method)_$(bhs[j].C)_$(bhs[j].ϵ0)_1.0_$(run_index).dist"),
        #      "w") do io
        #     write(io, "$run_index AID: $avg_i_d; AOD: $avg_o_d")
        # end
    end
end


function main()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--datanames", "-d"
            help = "input the data names, split by comma"
            arg_type = String
            required = false
        "--methods", "-m"
            help = "input the methods, split by comma"
            arg_type = String
            required = false
        "--num_repeats", "-l"
            help = "number of loops for repeating the simulations"
            arg_type = Int
            required = false
        "--max_loops"
            help = "number of loops for the MCMC"
            arg_type = Int
            required = true
        "--burnin"
            help = "Burn in number for MH sampler"
            arg_type = Int
            required = false
        "--alpha"
            help = "Hyperparameter alpha"
            arg_type = Float64
            required = false
        "--gamma"
            help = "Hyperparameter gamma"
            arg_type = Float64
            required = false
        "--eta"
            help = "Hyperparameter eta"
            arg_type = Float64
            required = false
        "--lambda"
            help = "Hyperparameter lambda for NIW"
            arg_type = Float64
            required = false
        "--nu"
            help = "Hyperparameter nu for NIW"
            arg_type = Float64
            required = false
        "--max_learning_runs"
            help = "The maximum learning runs"
            arg_type = Int
            required = false
        "--epsilon0"
            help = "Hyperparameter epsilon0"
            arg_type = Float64
            required = false
        "--nu0"
            help = "Hyperparameter nu0"
            arg_type = Float64
            required = false
        "--C"
            help = "Hyperparameter C"
            arg_type = Float64
            required = false
        "--disc"
            help = "Hyperparameter discount"
            arg_type = Float64
            required = false
        "--L"
            help = "The maximum length for the hierarchy"
            arg_type = Int
            required = false
        "--conv"
            help = "If tracking the convergence of the posterior"
            arg_type = Bool
            required = false
        "--ifreg"
            help = "If tracking the convergence of the posterior"
            arg_type = Bool
            required = false
        "--seed"
            help = "The random seed"
            arg_type = Int
            required = false
        "--savecooc"
            help = "If to save the coocurrence"
            arg_type = Bool
            required = false
        "--savehier4hqs"
            help = "If to save the hierarchy during the MCMC sampling phase after burnin"
            arg_type = Bool
            required = false
    end
    parsed_args = parse_args(s)

    # general settings for all methods

    burnin = parsed_args["burnin"] == nothing ? 5 : parsed_args["burnin"]

    alpha = parsed_args["alpha"] === nothing ? 1. : parsed_args["alpha"]

    gamma = parsed_args["gamma"] === nothing ? 8. : parsed_args["gamma"]

    eta = parsed_args["eta"] === nothing ? .1 : parsed_args["eta"]

    lambda = parsed_args["lambda"] === nothing ? .01 : parsed_args["lambda"]

    nu = parsed_args["nu"] === nothing ? 90 : parsed_args["nu"]

    epsilon0 = parsed_args["epsilon0"] === nothing ? 1 : parsed_args["epsilon0"]

    nu0 = parsed_args["nu0"] === nothing ? .01 : parsed_args["nu0"]

    C = parsed_args["C"] === nothing ? .1 : parsed_args["C"]

    disc = parsed_args["disc"] === nothing ? .75 : parsed_args["disc"]

    L = parsed_args["L"] === nothing ? 3 : parsed_args["L"]

    max_loops = parsed_args["max_loops"] === nothing ? 5000 : parsed_args["max_loops"]

    num_repeats = parsed_args["num_repeats"] === nothing ? 1 : parsed_args["num_repeats"]

    if_reg = parsed_args["ifreg"] === nothing ? false : parsed_args["ifreg"]

    datanames::Vector{String} = parsed_args["datanames"] == nothing ?
        ["animals"] :
        split(parsed_args["datanames"], ',')

    savecooc = parsed_args["savecooc"] === nothing ? false : parsed_args["savecooc"]

    save_hier_4_hqs = parsed_args["savehier4hqs"] === nothing ? false : parsed_args["savehier4hqs"]


    for dn in datanames
        if dn == "animals"
            Threads.@threads for i in 1:num_repeats
            # for i in 1:num_repeats
                try
                    test_animals(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                 ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                 max_loops=max_loops, savecooc=savecooc,
                                 save_hier_4_hqs=save_hier_4_hqs,
                                 run_index=i)
                catch err
                    println("ERROR: ", err)
                end
            end
        elseif dn == "compound"
            Threads.@threads for i in 1:num_repeats
            # for i in 1:num_repeats
                # try
                    test_compound(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                  ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                  max_loops=max_loops, savecooc=savecooc,
                                  save_hier_4_hqs=save_hier_4_hqs,
                                  run_index=i)
                # catch
                #     nothing
                # end
            end
        elseif dn == "fmnist"
            Threads.@threads for i in 1:num_repeats
                try
                    test_fmnist(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                max_loops=max_loops, savecooc=savecooc,
                                save_hier_4_hqs=save_hier_4_hqs,
                                run_index=i)
                catch err
                    println("ERROR: ", err)
                    test_fmnist(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                max_loops=max_loops, savecooc=savecooc,
                                save_hier_4_hqs=save_hier_4_hqs,
                                run_index=i)
                end
            end
        end
    end
end

@time main()
