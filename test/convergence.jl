using Clustering: hclust, Hclust, randindex, varinfo
using DataFrames
using Random
using SparseArrays
using ArgParse
using JLD2
using CSV
using LinearAlgebra
using PDMats
using Distributions
using DataStructures:Queue

path = normpath(joinpath(dirname(@__FILE__), "../src"))
push!(LOAD_PATH, path)
using BHMC

Random.seed!(2025)


@inline function test_animals(burnin::Int; kwargs...)
    raw_df = CSV.File("data/animals/animals_after_pca_7.csv";
                      delim=',', header=1) |> DataFrame
    X = BHMC.DoubleMat(raw_df[!, 2:end])
    names = Vector(raw_df[!, 1])
    S = bhmc_regmh(X; burnin=burnin, kwargs...)

    zs = NodeVec()
    zps = NodeVec()
    best_map_bh = VBMCMCHierarchy()
    best_mcle_bh = VBMCMCHierarchy()
    mp = l_post = -Inf
    mcl = l_mcl = -Inf
    mp_ind = 0
    mcl_ind = 0

    kwargs_dict = Dict(kwargs)
    loops = kwargs_dict[:max_loops] |> Int

    p_lkhds = []
    l_mcls = []
    n_n_nodes = []
    run_index = kwargs_dict[:run_index]
    dataname = "animals"

    bh = S.bh
    @inbounds for ind = 1:loops
        infer!(S, zs, zps, nothing, false)
        l_mcl, p_lkhd = log_mcle_separate(S.bh)
        push!(l_mcls, l_mcl)
        push!(p_lkhds, p_lkhd)
        hier = Hierarchy()
        convert2hierarchy!(hier, S.bh)
        push!(n_n_nodes, hier.m)
    end
    df = DataFrame([l_mcls, p_lkhds, n_n_nodes], [:mcl, :p_lkhd, :nnz])
    savedf(df, "$(dataname)_regmh_$(bh.ϵ0)_$(bh.C)_$(bh.ν0)_$(run_index).csv";
           path="insights/convergence")
end


"""
Test on fashion MNIST data
"""
function test_fmnist(burnin::Int; kwargs...)
    raw_df = CSV.File("data/fmnist/sampled_fmnist2.csv";
                       delim=',', header=1) |> DataFrame

    X = BHMC.DoubleMat(raw_df[!, 2:end-1])
    S = bhmc_regmh(X; burnin=burnin, kwargs...)

    zs = NodeVec()
    zps = NodeVec()
    best_map_bh = VBMCMCHierarchy()
    best_mcle_bh = VBMCMCHierarchy()
    mp = l_post = -Inf
    mcl = l_mcl = -Inf
    mp_ind = 0
    mcl_ind = 0

    kwargs_dict = Dict(kwargs)
    loops = kwargs_dict[:max_loops] |> Int

    p_lkhds = []
    l_mcls = []
    n_n_nodes = []
    run_index = kwargs_dict[:run_index]
    dataname = "fmnist"

    bh = S.bh
    @inbounds for ind = 1:loops
        infer!(S, zs, zps, nothing, false)
        l_mcl, p_lkhd = log_mcle_separate(S.bh)
        push!(l_mcls, l_mcl)
        push!(p_lkhds, p_lkhd)
        push!(n_n_nodes, nnz(bh.n_z_x))
    end
    df = DataFrame([l_mcls, p_lkhds, n_n_nodes], [:mcl, :p_lkhd, :nnz])
    savedf(df, "$(dataname)_regmh_$(bh.ϵ0)_$(bh.C)_$(bh.ν0)_$(run_index).csv";
           path="insights/convergence")
end


function main()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--datanames", "-d"
            help = "input the data names, split by comma"
            arg_type = String
            required = false
        "--methods", "-m"
            help = "input the methods, split by comma"
            arg_type = String
            required = false
        "--num_repeats", "-l"
            help = "number of loops for repeating the simulations"
            arg_type = Int
            required = false
        "--max_loops"
            help = "number of loops for the MCMC"
            arg_type = Int
            required = true
        "--burnin"
            help = "Burn in number for MH sampler"
            arg_type = Int
            required = false
        "--alpha"
            help = "Hyperparameter alpha"
            arg_type = Float64
            required = false
        "--gamma"
            help = "Hyperparameter gamma"
            arg_type = Float64
            required = false
        "--eta"
            help = "Hyperparameter eta"
            arg_type = Float64
            required = false
        "--lambda"
            help = "Hyperparameter lambda for NIW"
            arg_type = Float64
            required = false
        "--nu"
            help = "Hyperparameter nu for NIW"
            arg_type = Float64
            required = false
        "--max_learning_runs"
            help = "The maximum learning runs"
            arg_type = Int
            required = false
        "--epsilon0"
            help = "Hyperparameter epsilon0"
            arg_type = Float64
            required = false
        "--nu0"
            help = "Hyperparameter nu0"
            arg_type = Float64
            required = false
        "--C"
            help = "Hyperparameter C"
            arg_type = Float64
            required = false
        "--disc"
            help = "Hyperparameter discount"
            arg_type = Float64
            required = false
        "--L"
            help = "The maximum length for the hierarchy"
            arg_type = Int
            required = false
        "--conv"
            help = "If tracking the convergence of the posterior"
            arg_type = Bool
            required = false
        "--ifreg"
            help = "If tracking the convergence of the posterior"
            arg_type = Bool
            required = false
        "--seed"
            help = "The random seed"
            arg_type = Int
            required = false
    end
    parsed_args = parse_args(s)

    # general settings for all methods

    # methods::Vector{String} = parsed_args["methods"] == nothing ?
    #     ["gibbs"] :
    #     split(parsed_args["methods"], ",")

    burnin = parsed_args["burnin"] == nothing ? 5 : parsed_args["burnin"]

    alpha = parsed_args["alpha"] === nothing ? 1. : parsed_args["alpha"]

    gamma = parsed_args["gamma"] === nothing ? 8. : parsed_args["gamma"]

    eta = parsed_args["eta"] === nothing ? .1 : parsed_args["eta"]

    lambda = parsed_args["lambda"] === nothing ? .01 : parsed_args["lambda"]

    nu = parsed_args["nu"] === nothing ? 90 : parsed_args["nu"]

    epsilon0 = parsed_args["epsilon0"] === nothing ? 1 : parsed_args["epsilon0"]

    nu0 = parsed_args["nu0"] === nothing ? .01 : parsed_args["nu0"]

    C = parsed_args["C"] === nothing ? .1 : parsed_args["C"]

    disc = parsed_args["disc"] === nothing ? .75 : parsed_args["disc"]

    L = parsed_args["L"] === nothing ? 3 : parsed_args["L"]

    max_loops = parsed_args["max_loops"] === nothing ? 5000 : parsed_args["max_loops"]

    num_repeats = parsed_args["num_repeats"] === nothing ? 1 : parsed_args["num_repeats"]

    if_reg = parsed_args["ifreg"] === nothing ? false : parsed_args["ifreg"]

    datanames::Vector{String} = parsed_args["datanames"] == nothing ?
        ["animals"] :
        split(parsed_args["datanames"], ',')

    for dn in datanames
        if dn == "animals"
            Threads.@threads for i in 1:num_repeats
                try
                    test_animals(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                 ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                 max_loops=max_loops,
                                 run_index=i)
                catch
                    # try again -- ad hoc
                    test_animals(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                 ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                 max_loops=max_loops,
                                 run_index=i)
                end
            end
        elseif dn == "fmnist"
            Threads.@threads for i in 1:num_repeats
                try
                    test_fmnist(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                max_loops=max_loops,
                                run_index=i)
                catch
                    # try again -- ad hoc
                    test_fmnist(burnin; L=L, α=alpha, γ0=gamma, η=eta, λ=lambda, ν=nu,
                                ν0=nu0, ϵ0=epsilon0, C=C, if_reg=if_reg,
                                max_loops=max_loops,
                                run_index=i)
                end
            end
        end
    end
end

@time main()
