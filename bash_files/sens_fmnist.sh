for ep in 0.00001 0.0001 0.001 0.01 0.1 1 10 100
do
    for C in 0.00001 0.0001 0.001 0.01 0.1 1 10 100
    do
        julia --optimize=2 --color=yes test/sensitivity.jl --datanames=fmnist \
        --burnin=25000 --max_loops=25000 --num_repeats=30 \
        --alpha=.2 --gamma=1.5 --eta=.85 \
        --epsilon0=$ep --nu0=1 --C=$C --ifreg=true \
        --L=4
    done
done

julia --optimize=2 --color=yes test/sensitivity.jl --datanames=fmnist \
--burnin=25000 --max_loops=25000 --num_repeats=30 \
--alpha=.2 --gamma=1.5 --eta=.85 \
--epsilon0=0 --nu0=0 --C=0 --ifreg=false \
--L=4
