for ep in 1
do
    for C in 1
    do
        julia --optimize=2 --color=yes test/convergence.jl --datanames=fmnist \
        --burnin=0 --max_loops=50000 --num_repeats=4 \
        --alpha=.2 --gamma=1.5 --eta=.85 \
        --epsilon0=$ep --nu0=1 --C=$C --ifreg=true \
        --L=4
    done
done

