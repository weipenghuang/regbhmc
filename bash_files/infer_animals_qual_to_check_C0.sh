for ep in 0.0
do
    for C in 0.0000000000000000001
    do
        julia --optimize=3 --color=yes test/inferhier2.jl --datanames=animals \
        --burnin=25000 --max_loops=50000 --num_repeats=2 \
        --alpha=.4 --gamma=1 --eta=0.85 \
        --epsilon0=$ep --nu0=1 --C=$C --ifreg=true \
        --L=3 --savecooc=true --savehier4hqs=false

        #for idx in 1 2
        #do
        #    npython pyscript/plot_animals.py $idx $C $ep
        #done
    done
done

#julia --optimize=3 --color=yes test/inferhier2.jl --datanames=animals \
#--burnin=25000 --max_loops=50000 --num_repeats=10 \
#--alpha=.4 --gamma=1 --eta=0.85 \
#--epsilon0=0 --nu0=0 --C=0 --ifreg=false \
#--L=3 --savecooc=true --savehier4hqs=false
##for idx in 1 2
#do
#    npython pyscript/plot_animals.py $idx 0.0 0.0
#done
