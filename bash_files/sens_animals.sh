for ep in 0.001 0.01 0.1 1 10 100 1000
do
    for C in 0.001 0.01 0.1 1 10 100 1000
    do
        julia --optimize=3 --color=yes test/sensitivity.jl --datanames=animals \
        --burnin=25000 --max_loops=25000 --num_repeats=30 \
        --alpha=.4 --gamma=1 --eta=.85 \
        --epsilon0=$ep --nu0=1 --C=$C --ifreg=true \
        --L=3
    done
done

julia --optimize=3 --color=yes test/sensitivity.jl --datanames=animals \
--burnin=25000 --max_loops=25000 --num_repeats=30 \
--alpha=.4 --gamma=1 --eta=.85 \
--epsilon0=0 --nu0=0.1 --C=0 --ifreg=false \
--L=3
