for ep in 1.0 0.01
do
    for C in 0.0001
    do
        julia --optimize=3 --color=yes test/inferhier2.jl --datanames=fmnist \
        --burnin=50000 --max_loops=50000 --num_repeats=2 \
        --alpha=.2 --gamma=1.5 --eta=.85 \
        --epsilon0=$ep --nu0=1 --C=$C --ifreg=true \
        --L=4 --savecooc=false --savehier4hqs=true

        for idx in 1 
        do 
            npython pyscript/plot_fmnist.py $idx $C $ep
        done
    done
done

julia --optimize=3 --color=yes test/inferhier2.jl --datanames=fmnist \
--burnin=50000 --max_loops=50000 --num_repeats=2 \
--alpha=.2 --gamma=1.5 --eta=.85 \
--epsilon0=0 --nu0=0 --C=0 --ifreg=false \
--L=4 --savecooc=true --savehier4hqs=true
for idx in 1 2 
do
    npython pyscript/plot_fmnist.py $idx 0.0 0.0
done
