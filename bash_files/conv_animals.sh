for ep in 10
do
    for C in 10
    do
        julia --optimize=3 --color=yes test/convergence.jl --datanames=animals \
        --burnin=0 --max_loops=50000 --num_repeats=5 \
        --alpha=.4 --gamma=1 --eta=.85 \
        --epsilon0=$ep --nu0=1 --C=$C --ifreg=true \
        --L=3
    done
done
